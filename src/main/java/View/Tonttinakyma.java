/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.Tonttisuunnitelma;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import static javafx.geometry.Orientation.HORIZONTAL;
import static javafx.geometry.Orientation.VERTICAL;
import javafx.geometry.Pos;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.WindowEvent;
import javax.imageio.ImageIO;

/**
 *
 * @author Allupelaaja
 */
public class Tonttinakyma extends DefaultView {
    //4:3 ratio näytön koossa?
    static final double LEVEYS = 780;
    static final double KORKEUS = 600;
   // private static final int OLETUS = 1, ASFALTTI = 2, NURMIKKO = 3, SORA = 4;
    private Tonttisuunnitelma tontti;
    //Ohjaimen metodeja kutsutaan käyttiksen kautta
    //Ohjain ohjain = new Ohjain(kayttis);
    String piirtovalinta,elementtiNimi,alustaNimi;
    String[] viimeisinMuutosUndo = new String[4],viimeisinMuutosRedo = new String[4];
    int muutoksia;
    int poistettavaa;
    GridPane ruudukko;
    int ruudunpituus = 30,ruudunvali = 0,vanhaX = -1,vanhaY=-1;
    Text koordinaatit,budget,valittuKomponentti;
    Boolean elementtiMenu = false,alustaMenu = false;
    ArrayList<Button> elementtiList,alustaList,elementtiButtons;
    ScrollPane toolbarScroller;
    ArrayList<FlowPane> flowPanes;
    Object vanhaSource;
    
    /**
     *
     * @param kayttoliittyma
     * @param t
     */
    public Tonttinakyma(Kayttoliittyma kayttoliittyma, Tonttisuunnitelma t) {
        super(kayttoliittyma,LEVEYS,KORKEUS);
        tontti = t;
        kayttis.tallennaKuvat();
        
        ImageView iv3 = new ImageView(new Image("newbtn.png"));
        iv3.setFitHeight(25);
        iv3.setFitWidth(25);
        
        ImageView iv4 = new ImageView(new Image("redo.png"));
        iv4.setFitHeight(25);
        iv4.setFitWidth(25);
        
        ImageView iv5 = new ImageView(new Image("undo.png"));
        iv5.setFitHeight(25);
        iv5.setFitWidth(25);
        
        ImageView iv6 = new ImageView(new Image("save.png"));
        iv6.setFitHeight(25);
        iv6.setFitWidth(25);
        
        ImageView iv7 = new ImageView(new Image("eraser.png"));
        iv7.setFitHeight(25);
        iv7.setFitWidth(25);
        
        ImageView iv8 = new ImageView(new Image("loadkuva.png"));
        iv8.setFitHeight(25);
        iv8.setFitWidth(25);
        
        ImageView iv9 = new ImageView(new Image("budjettikuva.png"));
        iv9.setFitHeight(25);
        iv9.setFitWidth(25);
        
        //Undo-nappi
        Button undoBtn = new Button("", iv5);
        undoBtn.setTooltip(new Tooltip("Undo"));
        undoBtn.setOnMouseClicked((MouseEvent e)->{
            /*
            if(viimeisinMuutosUndo[0] == null){
                    return;
                }
            
            int x = Integer.parseInt(viimeisinMuutosUndo[0]);
            int y = Integer.parseInt(viimeisinMuutosUndo[1]);
            
            String[] komponentit = kayttis.haeRuudunKomponentit(x, y);
            viimeisinMuutosRedo[0] = viimeisinMuutosUndo[0];
            viimeisinMuutosRedo[1] = viimeisinMuutosUndo[1];
            viimeisinMuutosRedo[2] = komponentit[0];
            viimeisinMuutosRedo[3] = komponentit[1];
            
            kayttis.vaihdaRuutu(viimeisinMuutosUndo[0], viimeisinMuutosUndo[1], viimeisinMuutosUndo[2], viimeisinMuutosUndo[3]);

            // Piirrä ensin alusta
            if (viimeisinMuutosUndo[2] != null) {
                piirraRuutuun(x, y, viimeisinMuutosUndo[2]);
            } else {
                piirraRuutuun(x, y, "Pyyhi");
            }
            // Piirrä sitten elementti
            if (viimeisinMuutosUndo[3] != null) {
                piirraRuutuun(x, y, viimeisinMuutosUndo[3]);
            }
            // Pyyhitään ruutu, jos valittu pyyhkiminen
            if (viimeisinMuutosUndo[2] == null && viimeisinMuutosUndo[3] == null) {
                piirraRuutuun(x, y, "Pyyhi");
            }
            
            viimeisinMuutosUndo[0] = null;
            */
            ArrayList<String[]> uusisisalto = new ArrayList();
            muutoksia++;
            uusisisalto = t.haeMuutoslista();
            int lisaapoistettavaa = t.haeMuutosmaara();
            poistettavaa = poistettavaa + lisaapoistettavaa;
            if (uusisisalto == null) {
                return;
            }
            for (int i = 0; i < t.getLeveys(); i++) {
                for (int j = 0; j < t.getKorkeus(); j++) {
                    piirraRuutuun(i, j, "Pyyhi");
                }
            }
            for (int i = 0; i < uusisisalto.size() - poistettavaa; i++) {
                String[] ruutu = uusisisalto.get(i);
                int x = Integer.parseInt(ruutu[0]);
                int y = Integer.parseInt(ruutu[1]);
                if (ruutu[2] != null) {
                    piirraRuutuun(x, y, ruutu[2]);
                }
                if (ruutu[3] != null) {
                    piirraRuutuun(x, y, ruutu[3]);
                }
            }
        });
        
        //Redo-nappi
        Button redoBtn = new Button("", iv4);
        redoBtn.setTooltip(new Tooltip("Redo"));
        redoBtn.setOnMouseClicked((MouseEvent e)->{
            /*
            if(viimeisinMuutosRedo[0] == null){
                    return;
                }
            
            int x = Integer.parseInt(viimeisinMuutosRedo[0]);
            int y = Integer.parseInt(viimeisinMuutosRedo[1]);
            
            String[] komponentit = kayttis.haeRuudunKomponentit(x, y);
            viimeisinMuutosUndo[0] = viimeisinMuutosRedo[0];
            viimeisinMuutosUndo[1] = viimeisinMuutosRedo[1];
            viimeisinMuutosUndo[2] = komponentit[0];
            viimeisinMuutosUndo[3] = komponentit[1];
            
            kayttis.vaihdaRuutu(viimeisinMuutosRedo[0], viimeisinMuutosRedo[1], viimeisinMuutosRedo[2], viimeisinMuutosRedo[3]);

            // Piirrä ensin alusta
            if (viimeisinMuutosRedo[2] != null) {
                piirraRuutuun(x, y, viimeisinMuutosRedo[2]);
            } else {
                piirraRuutuun(x, y, "Pyyhi");
            }
            // Piirrä sitten elementti
            if (viimeisinMuutosRedo[3] != null) {
                piirraRuutuun(x, y, viimeisinMuutosRedo[3]);
            }
            // Pyyhitään ruutu, jos valittu pyyhkiminen
            if (viimeisinMuutosRedo[2] == null && viimeisinMuutosRedo[3] == null) {
                piirraRuutuun(x, y, "Pyyhi");
            }
            
            viimeisinMuutosRedo[0] = null;
            */
        });
        
        //Save-nappi
        Button saveBtn = new Button("", iv6);
        saveBtn.setTooltip(new Tooltip("Tallenna"));
        saveBtn.setOnMouseClicked(event -> {
            if(kayttis.getKirjautunutKayttaja().equals("Tuntematon")){
                System.out.println("Et voi tallentaa suunnitelmaa tuntemattomana");
                return;
            }
            TextInputDialog dialog = new TextInputDialog(tontti.getNimi());
            
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.setTitle("Suunnitelman nimi");
            dialog.setHeaderText("Aseta suunnitelmalle nimi");
            dialog.setContentText("Suunnitelman nimi:");
            dialog.initOwner(stage);
            Optional<String> result;
            try{
                do {
                    result = dialog.showAndWait();
                    if(result.get().length() == 0){
                        dialog.setHeaderText("Suunnitelman nimi ei voi olla tyhjä");
                        System.out.println("Suunnitelman nimi ei voi olla tyhjä");
                    }
                } while(result.get().length() == 0);
                
            }catch(Exception e){
                result = null;
            }
            if(result == null){
                return;
            }
            if (result.get().length() == 0 || !result.isPresent()){
                System.out.println("Sunnitelmaa ei tallennettu");
            } else {
                if(!result.get().equals(tontti.getNimi())){
                    tontti.setNimi(result.get());
                }
                File file;
                byte [] kuvaBytes;
                try {
                    WritableImage image = ruudukko.snapshot(new SnapshotParameters(), null);
                    file = new File("SuunnitelmaKuvat/"+tontti.getNimi()+".png");
                    ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
                    kuvaBytes = Files.readAllBytes(file.toPath());
                    tontti.setKuva(kuvaBytes);
                    System.out.println(file.getName()+" tallennettu suunnitelman kuvaksi");
                } catch (IOException e) {
                    System.out.println("Sunnitelman kuvan asettamisessa ongelmia");
                    e.printStackTrace();
                }
                kayttis.setOmistaja(kayttis.getKirjautunutKayttaja());
                ;
                if(kayttis.tallennaSuunnitelma(tontti)){
                    stage.hide();
                    kayttis.hideEtusivuStage();//Tonttisuunnitelmat päivitetään etusivulla
                    kayttis.showEtusivuStage(); 
                } else {
                    System.out.println("Tallentaminen epäonnistui");
                }
            }
            
            });
        //Uusi suunnitelma -nappi
        Button newPlanBtn = new Button("", iv3);
        newPlanBtn.setTooltip(new Tooltip("Uusi tonttisuunnitelma"));
        
        Button pyyhiNappi = new Button("", iv7);
        pyyhiNappi.setTooltip(new Tooltip("Kumi"));
        pyyhiNappi.setOnMouseClicked((MouseEvent e)-> {
           piirtovalinta = "Pyyhi";
           alustaNimi = null;
           elementtiNimi = null;
           System.out.println(piirtovalinta);
           valittuKomponentti.setText(piirtovalinta);
        });
        
        Button budgetBtn = new Button("", iv9);
        budgetBtn.setTooltip(new Tooltip("Budjetti"));
        
        Button loadBtn = new Button("", iv8);
        loadBtn.setTooltip(new Tooltip("Avaa tiedosto"));
        
        loadBtn.setOnMouseClicked(event -> {
            kayttis.showSuunitelmaValikkoStage();
        });
        
        vaakaToolBar = new ToolBar(
                settingsBtn,
                new Separator(HORIZONTAL),
                backBtn,
                new Separator(HORIZONTAL),
                newPlanBtn,
                loadBtn,
                saveBtn,
                new Separator(HORIZONTAL),
                undoBtn,
                redoBtn,
                new Separator(HORIZONTAL),
                pyyhiNappi,
                new Separator(HORIZONTAL),
                budgetBtn,
                spacer,
                yllapitoBtn,
                new Separator(HORIZONTAL),
                username,
                logoutBtn
                
        );
        
        borderPane.setTop(vaakaToolBar);
        
        
        try {
            elementtiButtons =  kayttis.getElementtiButtons();
            flowPanes = kayttis.getMateriaaliPanet();
            ToolBar leftToolBar = new ToolBar();
            leftToolBar.setOrientation(VERTICAL);
            borderPane.setLeft(leftToolBar);
        
            flowPanes.forEach((pane) -> {
                pane.getChildren().forEach((n) -> {
                    n.setOnMouseClicked((MouseEvent e) -> {
                        piirtovalinta = ((Button)e.getSource()).getText();
                        if (elementtiButtons.get(flowPanes.indexOf(pane)).getText().equals("Alusta")) {
                            alustaNimi = ((Button)e.getSource()).getText();
                            elementtiNimi = null;
                        } else {
                            alustaNimi = null;
                            elementtiNimi = ((Button)e.getSource()).getText();
                        }
                        //System.out.println("Materiaalin nimi: "+piirtovalinta+"\nElementin tyyppi: "+elementtiButtons.get(flowPanes.indexOf(pane)).getText());
                        valittuKomponentti.setText("Materiaalin nimi: "+piirtovalinta+"\nElementin tyyppi: "+elementtiButtons.get(flowPanes.indexOf(pane)).getText());
                    });
                });
            });

            elementtiButtons.forEach((b) -> {
                b.setOnMouseClicked((MouseEvent e) -> {
                    toolbarScroller.setContent(flowPanes.get(elementtiButtons.indexOf(b)));
                });
                leftToolBar.getItems().add(b);
            });
        
        
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Buttonien asettamisessa ongelmia");
        }
        
        toolbarScroller = new ScrollPane();
        
        toolbarScroller.setMaxHeight(55);
        BorderPane sisempiBP = new BorderPane();
        sisempiBP.setTop(toolbarScroller);
        toolbarScroller.setFitToWidth(true);
        //Grid
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10); //Leveys
        grid.setVgap(10); //Korkeus
        grid.setPadding(new Insets(25, 25, 25, 25));
        
        //Tekstikentät
        Text scenetitle = new Text();
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);
        
        if(tontti != null && !tontti.getNimi().equals("")){
            stage.setTitle("Suunnitelma: "+tontti.getNimi());
            scenetitle.setText("Suunnitelma: "+tontti.getNimi());
        } else {
            stage.setTitle("Uusi suunnitelma");
            scenetitle.setText("Uusi suunnitelma");
        }
        //Muodostetaan ruudukon asetukset
        ruudukko = new GridPane();
        ruudukko.setHgap(ruudunvali);
        ruudukko.setVgap(ruudunvali);
        
        //Lisää vieritettävät palkit, mikäli ruudukko ei näy kokonaan
        ScrollPane sp = new ScrollPane();
        sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        sp.setContent(ruudukko);
        
        grid.add(sp, 0, 1);
        
        //Ruudukon koordinaatit
        koordinaatit = new Text("");
        koordinaatit.setFont(Font.font("Tahoma", FontWeight.NORMAL, 16));
        
        //Valitun komponentin näyttäminen
        valittuKomponentti = new Text("");
        valittuKomponentti.setFont(Font.font("Tahoma", FontWeight.NORMAL, 16));
        
        budget = new Text("");
        
        
        
        //Järjestetään aputekstit riviin ruudukon alle
        HBox hbox = new HBox();
        hbox.setSpacing(20);
        hbox.getChildren().addAll(koordinaatit,valittuKomponentti,budget);
        
    /*    HBox hbox1 = new HBox();
        hbox1.setSpacing(20);
        hbox1.getChildren().add(budget); */
        
                
        //grid.add(hbox, 0, 2);
        
        sisempiBP.setCenter(grid);
        sisempiBP.setBottom(hbox);
        hbox.setAlignment(Pos.CENTER);
        borderPane.setCenter(sisempiBP);
            
        
        //Ruudukon tapahtumakäsittelijät
        ruudukko.addEventHandler(MouseEvent.MOUSE_MOVED, (MouseEvent e) -> {
            int leveyspikseli = (int)e.getX();
            int x = laskeRuudunKoordinaatti(leveyspikseli);
            int korkeuspikseli = (int)e.getY();
            int y = laskeRuudunKoordinaatti(korkeuspikseli);
            koordinaatit.setText("(" + x + ", " + y + ")");
            budget.setText("Budjetti " + haeBudjetti()+" € ");
        });
        
        ruudukko.addEventHandler(MouseEvent.MOUSE_RELEASED, (MouseEvent e) -> {
            t.tallennaMuutosmaara();
        });
        
        
        ruudukko.setOnMousePressed((MouseEvent e) ->{
             mousePressed(e);
        });
        ruudukko.setOnMouseDragged((MouseEvent e) ->{
             mousePressed(e);
        });
        
        stage.setOnShowing((WindowEvent event) -> {
            defaultOnLoadActions();
            loadBtn.setDisable(kayttis.getKirjautunutKayttaja().equals("Tuntematon"));
            saveBtn.setDisable(kayttis.getKirjautunutKayttaja().equals("Tuntematon"));
            //indeksit    0, 1, 2,       3
            //String[] = [X, Y, ALUSTA, ELEMENTTI}
            ArrayList<String[]> sisalto = tontti.haeSisaltoBlob();
            tontti.asetaSisalto(sisalto);
//            tontti.setBlobArray(sisalto);
//            System.out.println("Sisalto size: "+sisalto.size());
            for (int x = 0; x < tontti.getKorkeus(); x++) {
                for (int y = 0; y < tontti.getLeveys(); y++) {
                    Rectangle ruutu = new Rectangle();
                    ruutu.setHeight(ruudunpituus);
                    ruutu.setWidth(ruudunpituus);
                    
                    String alusta = kayttis.getMaterialPicName(tontti.getAlusta());
                    if(alusta != null){
                        ruutu.setFill(new ImagePattern(new Image("file:"+alusta)));
                    } else {
                        ruutu.setFill(Color.WHITE);
                    }
                    
                    ruudukko.add(ruutu, x, y);
                }
            }
            //indeksit    0, 1, 2,       3
            //String[] = [X, Y, ALUSTA, ELEMENTTI}
            sisalto.forEach((s) -> {
                
                int nykX = Integer.parseInt(s[0]);
                int nykY = Integer.parseInt(s[1]);
                String nykAlusta =          s[2];
                String nykElementti =       s[3];

                String nykAlustaKuva = kayttis.getMaterialPicName(nykAlusta);
                if(nykAlustaKuva != null){
                    Rectangle alustaRuutu = new Rectangle();
                    alustaRuutu.setHeight(ruudunpituus);
                    alustaRuutu.setWidth(ruudunpituus);
                    alustaRuutu.setFill(new ImagePattern(new Image("file:"+nykAlustaKuva)));
                    ruudukko.add(alustaRuutu, nykX, nykY);
                }
                Rectangle elementtiRuutu = new Rectangle();
                elementtiRuutu.setHeight(ruudunpituus);
                elementtiRuutu.setWidth(ruudunpituus);
                String nykElementtiKuva = kayttis.getMaterialPicName(nykElementti);
                if(nykElementtiKuva != null){
                    elementtiRuutu.setFill(new ImagePattern(new Image("file:"+nykElementtiKuva)));
                    ruudukko.add(elementtiRuutu, nykX, nykY);
                }
            });
//            System.out.println("Sisalto size: "+tontti.haeSisaltoArray().size());
//            System.out.println("Blob size: "+tontti.haeBlobArray().length);
        });
        
        //NAPPIEN ACTIONIT
        
        //Budjettinapin action
        budgetBtn.setOnAction((ActionEvent e) -> {
            kayttis.showBudjettiStage(tontti);
        });
        
        //Uusi suunnitelma -napin action
        newPlanBtn.setOnAction((ActionEvent e) -> {
            kayttis.showMaarittelyStage();
        });
        
    }
    
    /**
     *
     * @param tontti
     */
    public void setTontti(Tonttisuunnitelma tontti) {
        tontti = tontti;
    }
    public Tonttisuunnitelma getTontti () {
        return tontti;
    }
    
    /**
     * Lisää ruudukkoon valitun elementin tai suorittaa toiminnon halutussa ruudussa
     * 
     * @param x ruudukon leveyskoordinaatti
     * @param y ruudukon korkeuskoordinaatti
     */
    public void lisaaRuudukkoon(int x, int y) {
        if(piirtovalinta == null){
                    return;
                }
        
        String strX = Integer.toString(x);
        String strY = Integer.toString(y);
        
        String[] komponentit = kayttis.haeRuudunKomponentit(x, y);
        
        viimeisinMuutosUndo[0] = strX;
        viimeisinMuutosUndo[1] = strY;
        viimeisinMuutosUndo[2] = komponentit[0];
        viimeisinMuutosUndo[3] = komponentit[1];
        viimeisinMuutosRedo[0] = null;
        
        muutoksia = 0;
        poistettavaa = 0;
        
        kayttis.lisaaRuutuun(strX, strY, alustaNimi, elementtiNimi);
        komponentit = kayttis.haeRuudunKomponentit(x, y);
        
        // Piirrä ensin alusta
        if (komponentit[0] != null) {
            piirraRuutuun(x, y, komponentit[0]);
            tontti.lisaa(30.0);
        } else {
            piirraRuutuun(x, y, "Pyyhi");
            tontti.lisaa(30.0);
        }
        // Piirrä sitten elementti
        if (komponentit[1] != null) {
            piirraRuutuun(x, y, komponentit[1]);
            tontti.lisaa(30.0);
        }
        // Pyyhitään ruutu, jos valittu pyyhkiminen
        if (komponentit[0] == null && komponentit[1] == null) {
            piirraRuutuun(x, y, "Pyyhi");
            
        }
    }
    
    public void piirraRuutuun(int x, int y, String kuva) {
        Rectangle ruutu = new Rectangle();
        ruutu.setHeight(ruudunpituus);
        ruutu.setWidth(ruudunpituus);
        
        
        if (kuva.equals("Pyyhi")) {
            String alusta = kayttis.getMaterialPicName(tontti.getAlusta());
            if(alusta != null){
                ruutu.setFill(new ImagePattern(new Image("file:"+alusta)));
                tontti.palauta(30.0);
            } else {
                ruutu.setFill(Color.WHITE);
                tontti.palauta(30.0);
            }
        } else {
            String kuvanNimi = kayttis.getMaterialPicName(kuva);
            if(kuvanNimi == null){
                System.out.println("Materiaalin: "+kuva+ " kuvan nimeä ei löytynyt");
                return;
            } else {
                Image img = new Image("file:"+kuvanNimi);
                ruutu.setFill(new ImagePattern(img));
                //System.out.println("Sisältö: "+ tontti.getSisalto().size());
            }
            
        }
        
        ruudukko.add(ruutu, x, y);
    }
    
    public int laskeRuudunKoordinaatti(int pikseli) {
        int koordinaatti = pikseli/(ruudunpituus+ruudunvali);
        return koordinaatti;
    }
    
    public String haeBudjetti() {
        return Double.toString(tontti.getBudjetti());
    }
    
    public int tarkistaX(int x) {
        if (x > tontti.getLeveys()-1) {
            x = (int)tontti.getLeveys()-1;
        }
        if (x < 0) {
            x = 0;
        }   
        return x;
    }
    
    public int tarkistaY(int y) {
        if (y > tontti.getKorkeus()-1) {
            y = (int)tontti.getKorkeus()-1;
        }
        if (y < 0) {
            y = 0;
        }
        return y;
    }
    
    public boolean ehkaiseTuplat(int x, int y) {
        boolean tuplat = true;
        String[] komponentit = kayttis.haeRuudunKomponentit(x, y);
        if (alustaNimi == null && elementtiNimi == null) {
            if (komponentit[0] == null && komponentit[1] == null) {
                
            } else {
                tuplat = false;
            }
        } else if ((komponentit[0] == null && komponentit[1] == null)) {
            tuplat = false;
        } else if (komponentit[0] == null && komponentit[1] != null) {
            if (!(komponentit[1].equals(elementtiNimi))) {
                tuplat = false;
            }
        } else if (komponentit[0] != null && komponentit[1] == null) {
            if (!(komponentit[0].equals(alustaNimi))) {
                tuplat = false;
            }
        } else if (komponentit[0] != null && komponentit[1] != null) {
            if ((komponentit[0].equals(alustaNimi)) || (komponentit[1].equals(elementtiNimi)) ) {

            } else {
                tuplat = false;
            }
        }
        return tuplat;
    }
    
    private void mousePressed(MouseEvent e) {
        int leveyspikseli = (int)e.getX();
        int korkeuspikseli = (int)e.getY();
        int x = laskeRuudunKoordinaatti(leveyspikseli);
        int y = laskeRuudunKoordinaatti(korkeuspikseli);
        
        if(vanhaX == x && vanhaY == y && vanhaSource == e.getSource()){
            return;
        }
        x = tarkistaX(x);
        y = tarkistaY(y);
        if (!ehkaiseTuplat(x, y)) {
            vanhaX = x;
            vanhaY = y;
            vanhaSource = e.getSceneX();
            lisaaRuudukkoon(x, y);
    }
}

    @Override
    void backButtonAction() {
        stage.hide();
        kayttis.showEtusivuStage();
    }

    @Override
    protected void alertAction() {
        backButtonAction();
    }
}
