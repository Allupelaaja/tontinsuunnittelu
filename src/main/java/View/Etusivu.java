/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import static javafx.geometry.Orientation.HORIZONTAL;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.paint.Color;
import javafx.stage.WindowEvent;

/**
 *
 * @author Allupelaaja
 */
public class Etusivu  extends DefaultView {
    

    private FlowPane flowPane;
    protected static final double LEVEYS = 550;
    protected static final double KORKEUS = 500;
    /**
     *
     * @param kayttoliittyma
     */
    public Etusivu(Kayttoliittyma kayttoliittyma) {
       super(kayttoliittyma,LEVEYS,KORKEUS);

       backBtn.setDisable(true);
       //Stage
       stage.setTitle("Etusivu");
       
       //Grid-iso
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10); //Leveys
        grid.setVgap(10); //Korkeus
//        grid.setPadding(new Insets(25, 25, 25, 25));
        BorderPane innerBP = new BorderPane();
        borderPane.setCenter(innerBP);
        innerBP.setCenter(grid);

        
        HBox topBar = new HBox();
        topBar.setAlignment(Pos.CENTER);
        topBar.setPadding(new Insets(10,10,10,10));
        topBar.setSpacing(10);
        innerBP.setTop(topBar);
        //Tekstikentät
        Text scenetitle = new Text("Etusivu");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
//        gridSmallUp.add(scenetitle, 1, 0, 2, 1);
       
        
//        gridSmallUp.add(actiontarget, 0, 4);
        

        GridPane gridSuunitelmat = new GridPane();
        gridSuunitelmat.setAlignment(Pos.CENTER);
        gridSuunitelmat.setHgap(10); //Leveys
        gridSuunitelmat.setVgap(10); //Korkeus
        gridSuunitelmat.setPadding(new Insets(25, 25, 25, 25));
        
        Text middleTitle = new Text("Valitse suunnitelma");
        middleTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        
        HBox titleHb = new HBox();
        titleHb.getChildren().add(middleTitle);
        titleHb.setAlignment(Pos.CENTER);
        gridSuunitelmat.add(titleHb, 0, 0, 2, 1);
        
        flowPane = new FlowPane();
        flowPane.setPadding(new Insets(10, 10, 10,10));
        flowPane.setVgap(4);
        flowPane.setHgap(4);
        
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(flowPane);
        gridSuunitelmat.add(scrollPane, 0, 1);
        
        //toolbarScroller.setMaxHeight(55);
//        scrollPane.setFitToWidth(true);
//        scrollPane.setFitToHeight(true);
        
        grid.add(gridSuunitelmat, 0, 1, 3, 2);
        //
        // NAPIT
        //
        
        //Käyttäjäsivunappi
        Button userBtn = new Button("Käyttäjäsivulle");
        userBtn.setId("userBtn");
        
        //Nappi uudelle suunnitelmalle
        Button planBtn = new Button("Uusi suunnitelma");
        planBtn.setId("planBtn");
        
        //Nappi gallerialle
        Button galleryBtn = new Button("Galleria");

        topBar.getChildren().addAll(scenetitle,planBtn,galleryBtn);
        
        //
        //NAPPIEN ACTIONIT
        //

        
        //Käyttäjäsivunapin action
        userBtn.setOnAction((ActionEvent e) -> {
            if (!kayttis.getKirjautunutKayttaja().equals("Tuntematon")) {
                stage.hide();
                kayttis.showKayttajaStage();
            }
       });
        
        vaakaToolBar = new ToolBar(
                settingsBtn,
                new Separator(HORIZONTAL),
                backBtn,
                new Separator(HORIZONTAL),
                spacer,
                userBtn,
                yllapitoBtn,
                new Separator(HORIZONTAL),
                username,
                logoutBtn
        );
        
        borderPane.setTop(vaakaToolBar);
        //Gallerianapin action
        galleryBtn.setOnAction((ActionEvent e) -> {
            kayttis.showGalleriaStage();
       });
        
        //Uuden suunnitelman luomisaction
        planBtn.setOnAction((ActionEvent e) -> {
            kayttis.showMaarittelyStage();
        });
        stage.setOnCloseRequest(event -> {
                alertAction();
        });
        
       stage.setOnShowing((WindowEvent event) -> {
            defaultOnLoadActions();
            userBtn.setDisable(kayttis.getKirjautunutKayttaja().equals("Tuntematon"));
            defaultOnLoadActions();
            updateFlowPane();
            
            gridSuunitelmat.setVisible(!kayttis.getKirjautunutKayttaja().equals("Tuntematon"));
            gridSuunitelmat.setManaged(!kayttis.getKirjautunutKayttaja().equals("Tuntematon"));
       });
    }
    private void updateFlowPane() {
        flowPane.getChildren().clear();
        ArrayList<GridPane> grids = kayttis.getSuunnitelmaGrids();
        if(grids.isEmpty()){
            return;
        }
            for(GridPane g : grids){
                //      0       1      2        3       4
                //g : [HBox,ImageView,openBtn,delBtn,shareBtn]
                String nimi = ((Text)((HBox) g.getChildren().get(0)).getChildren().get(0)).getText();
                    Button openBtn = (Button) g.getChildren().get(2);
                    Button delBtn = (Button) g.getChildren().get(3);
                    Button shareBtn = (Button) g.getChildren().get(4);
                
                //Open button
                openBtn.setOnMouseClicked(e -> {
//                   String omistaja = ((Text)((HBox) g.getChildren().get(5)).getChildren().get(0)).getText();
                   kayttis.showTonttinakymaStage(kayttis.getSuunnitelma(nimi));
                });
                
                //Delete button
                delBtn.setOnMouseClicked(e -> {
                    kayttis.poistaSuunnitelma(nimi);
                    updateFlowPane();
                });
                
                //Share button
                shareBtn.setOnMouseClicked(e -> {
                    kayttis.jaaSuunnitelma(nimi);
                    updateFlowPane();
                });
                flowPane.getChildren().add(g);
            }
    }
    @Override
    void backButtonAction() {
        logoutButtonAction();
    }
    @Override
    protected void resetStage() {
//        actiontarget.setText("");//Clearataan error kun siirrytään pois
    flowPane.getChildren().clear();
    }
    
}
