package View;

import Model.Tonttisuunnitelma;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.WindowEvent;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Matti
 */
public class Tontinmaarittely extends DefaultView{
    static final double LEVEYS = 320;
    static final double KORKEUS = 320;
    public Tonttisuunnitelma tontti;
    private Boolean etenemisCheck = true;
    private TextField width;
    private ComboBox combo;
    private String budjetti;
    private TextField budjetTextField;
    private TextField height;
        
            
           
    /**
     *
     * @param kayttoliittyma luo uuden käyttöliittymän
     * 
     */
    public Tontinmaarittely(Kayttoliittyma kayttoliittyma) {
        super(kayttoliittyma,LEVEYS,KORKEUS);
        yllapitoBtn.setVisible(false);
        yllapitoBtn.setManaged(false);
        kayttis.setTontti(tontti);
        //Stage
        stage.setTitle("Tontin määrittely");
        
        //Grid
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10); //Leveys
        grid.setVgap(10); //Korkeus
        grid.setPadding(new Insets(25, 25, 25, 25));
        
//        BaseGrid grid = GridFactory.createBaseGrid();
//        //Tai
//        CustomGrid grid = GridFactory.createCustomGrid(10,10,25);
        
        borderPane.setCenter(grid);
        
        Text scenetitle = new Text("Tontinsuunnittelu");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);
        
        Label budget = new Label("Budjetti(€)");
        grid.add(budget, 0, 3);
        
        budjetTextField = new TextField();
        grid.add(budjetTextField, 1, 3);
        budjetTextField.setId("sizeField");
        
        Label leveys = new Label("Leveys(m)");
        grid.add(leveys, 0, 1);
        
        width = new TextField();
        grid.add(width, 1, 1);
        width.setId("widthField");
        
        
        Label korkeus = new Label("Korkeus(m)");
        grid.add(korkeus, 0, 2);
        
        height = new TextField();
        grid.add(height, 1, 2);
        height.setId("heightField");
        
        Label alusta = new Label("Alusta");
        grid.add(alusta, 0, 4);

        Button acceptBtn = new Button("Hyväksy");
        HBox acceptHbBtn = new HBox(10);
        acceptHbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        acceptHbBtn.getChildren().add(acceptBtn);
        grid.add(acceptHbBtn, 1, 4);
        acceptBtn.setId("acceptBtn");
        
        //Poissakäytöstä toistaiseksi
//        Button budgetBtn = new Button("Määritä budjetti");
//        HBox budgetHbBtn = new HBox(10);
//        budgetHbBtn.setAlignment(Pos.BOTTOM_LEFT);
//        budgetHbBtn.getChildren().add(budgetBtn);
//        grid.add(budgetHbBtn, 0, 4);
        
        combo = new ComboBox();
        grid.add(combo, 1,4);
        combo.getItems().add("Oletus");
        kayttis.getAlustat().forEach((s) -> {
            combo.getItems().add(s);
        });
        //Poissakäytöstä toistaiseksi
//        budgetBtn.setOnAction((ActionEvent e) -> {
//            kayttoliittyma.showBudjettiStage();
//        });
        
        acceptBtn.setOnAction((ActionEvent e) -> {
            tontti = new Tonttisuunnitelma();
            etenemisCheck = true;
            String value;
            try {
                value = (String)combo.getValue();
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("Alustan asettamisessa ongelmia");
                return;
            }
            
            tontti.setAlusta(value);
            //System.out.println("Alusta: "+tontti.getAlusta());
            //Kommentoitu toistaiseksi, kunnes pinta-alaa käytetään
//            String koko1 = size.getText();
//            try {
//                tontti.setKoko(Double.parseDouble(koko1));
//                System.out.println(tontti.getKoko());
//            }catch(Exception exp) {
//                //Pois käytöstä toistaiseksi
//                //etenemisCheck = false;
//                System.out.println("Pinta-alaa ei asetettu (Pois käytöstä toistaiseksi)");
//            }

            budjetti = budjetTextField.getText();
            try {
                double suunnitelmaBudjetti = Double.parseDouble(budjetti);
                if(suunnitelmaBudjetti > 1000000 || suunnitelmaBudjetti <= 0){
                    etenemisCheck = false;
                    System.out.println("Budjetti on liian suuri tai negatiivinen \n"
                            + "Budjetin pitää olla väliltä 0 - 1 000 000");
                    return;
                }
                tontti.setBudjetti(Double.parseDouble(budjetti));
                //System.out.println("Budjetti: "+ tontti.getBudjetti());
            }catch(NumberFormatException exp) {
                    
                //etenemisCheck = false; 
                System.out.println("Budjettia ei asetettu (Ei pakollinen)");
            }
            String x = width.getText();
            try {
                double suunnitelmaLeveys = Double.parseDouble(x);
                if(suunnitelmaLeveys > 100 || suunnitelmaLeveys <= 0){
                    etenemisCheck = false;
                    System.out.println("Leveys on liian suuri tai negatiivinen\n"
                            + "Pitää olla välillä 0 - 100");
                    return;
                }
                tontti.setLeveys(suunnitelmaLeveys);
               // System.out.println("Leveys: "+tontti.getLeveys());
            }
            catch(NumberFormatException exp) {
                etenemisCheck = false;
                System.out.println("Leveyttä ei asetettu");
            }
            String y = height.getText();
            try {
                double suunnitelmaKorkeus = Double.parseDouble(y);
                if(suunnitelmaKorkeus > 100 || suunnitelmaKorkeus <= 0){
                    etenemisCheck = false;
                    System.out.println("Korkeus on liian suuri tai negatiivinen"
                            + "Pitää olla välillä 0 - 100");
                    return;
                }
                tontti.setKorkeus(Double.parseDouble(y));
                //System.out.println("Korkeus: "+tontti.getKorkeus());
            }
            catch(NumberFormatException exp) {
                etenemisCheck = false;
                System.out.println("Korkeutta ei asetettu");
            }
            
            if (etenemisCheck) {
                //Kommentissa toistaiseksi kunnes tarvitaan debuggia varten
//                    Alert newAlert = new Alert(AlertType.INFORMATION);
//                    newAlert.setTitle("Valittu alusta");
//                    newAlert.setHeaderText(null);
//                    newAlert.setContentText("Valittu alusta: " + value);
//                    newAlert.showAndWait();
                budjetTextField.clear();
                width.clear();
                height.clear();
                combo.getSelectionModel().selectFirst();
//                String omistaja = kayttis.getKirjautunutKayttaja();
//                if(!omistaja.equals("Tuntematon")){
//                    kayttis.setOmistaja(omistaja);
//                }
                stage.hide();
                kayttis.showTonttinakymaStage(tontti);
                
            } else {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Määrittelytiedoissa on vikaa");
                alert.setHeaderText(null);
                alert.setContentText("Syöttämissäsi tiedoissa on puutteita tai tiedot eivät ole kelvollisia!");
                alert.showAndWait();
            }
        });
        //Luo uuden tonttisuunnitelman enteristä
        scene.addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent t) -> {
            if(t.getCode()==KeyCode.ENTER) {
                acceptBtn.fire();
            }
        });
        //"focusaa" tonttin leveyden textfieldin aluksi.
        stage.setOnShowing((WindowEvent event) -> {
            defaultOnLoadActions();
            combo.getSelectionModel().selectFirst();
            width.requestFocus();
        });
        stage.setOnCloseRequest(event -> {
//                backButtonAction();
        });
    }

    @Override
    void backButtonAction() {
        stage.hide();
        kayttis.showEtusivuStage();
    }
    @Override
    protected void resetStage () {
        budjetTextField.clear();
        width.clear();
        height.clear();
        combo.getSelectionModel().selectFirst();
    }
}
