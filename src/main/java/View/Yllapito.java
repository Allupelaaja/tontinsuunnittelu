/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;



import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.WindowEvent;

/**
 *
 * @author Allupelaaja
 */
public class Yllapito extends DefaultView {
    private final static double LEVEYS = 400;
    private final static double KORKEUS = 490;
    
    TableView tableKayttajat;
    TableView tableElementit;
    TableView tableMateriaalit;
    TableView tableSuunnitelmat;
    FileChooser fileChooser;
    File valittuMaterialFile;
    File valittuElementFile;
    TextField removePlanId,addMaterialEType,
            addMaterialPrice, addElementName,
            addElementType,removeUserId,
            changeUserRightId,addMaterialName,
            delMaterialId,delElementId;
    ComboBox combo;
    /**
     *
     * @param kayttoliittyma
     */
    public Yllapito(Kayttoliittyma kayttoliittyma)  {
        super(kayttoliittyma,LEVEYS,KORKEUS);
        this.fileChooser = new FileChooser();
        this.tableMateriaalit = new TableView();
        this.tableElementit = new TableView();
        this.tableKayttajat = new TableView();
        this.tableSuunnitelmat = new TableView();
        
        fileChooser.setInitialDirectory(
            new File(System.getProperty("user.home"))
        );
        fileChooser.setTitle("Etsi kuva");
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("PNG", "*.png")
        );
            
        Tab kayttajaTab = new Tab("Käyttäjät"), elementtiTab = new Tab("Elementit"), materiaaliTab = new Tab("Materiaalit"),suunnitelmaTab = new Tab("Suunnitelmat");
        TabPane tabPane = new TabPane(); 
        tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
        tabPane.getTabs().addAll(kayttajaTab,elementtiTab,materiaaliTab,suunnitelmaTab);
        //Stage
        stage.setTitle("Yllapito");
        
        //Kayttaja gridi
        GridPane gridKayttaja = new GridPane();
        gridKayttaja.setAlignment(Pos.CENTER);
        gridKayttaja.setHgap(10); //Leveys
        gridKayttaja.setVgap(10); //Korkeus
        gridKayttaja.setPadding(new Insets(25, 25, 25, 25));
                
        //Elementti gridi
        GridPane gridElementti = new GridPane();
        gridElementti.setAlignment(Pos.CENTER);
        gridElementti.setHgap(10); //Leveys
        gridElementti.setVgap(10); //Korkeus
        gridElementti.setPadding(new Insets(25, 25, 25, 25));
        
        //Materiaali gridi
        GridPane gridMateriaali = new GridPane();
        gridMateriaali.setAlignment(Pos.CENTER);
        gridMateriaali.setHgap(10); //Leveys
        gridMateriaali.setVgap(10); //Korkeus
        gridMateriaali.setPadding(new Insets(25, 25, 25, 25));
        
        //Suunnittelu gridi
        GridPane gridSuunnitelma = new GridPane();
        gridSuunnitelma.setAlignment(Pos.CENTER);
        gridSuunnitelma.setHgap(10); //Leveys
        gridSuunnitelma.setVgap(10); //Korkeus
        gridSuunnitelma.setPadding(new Insets(25, 25, 25, 25));
        
        //Tekstikentät
        Text scenetitleKayttajat = new Text("Käyttäjät");
        scenetitleKayttajat.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        gridKayttaja.add(scenetitleKayttajat, 0, 0, 2, 1);
        
        Text scenetitleElementit = new Text("Elementit");
        scenetitleElementit.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        gridElementti.add(scenetitleElementit, 0, 0, 2, 1);
        
        Text scenetitleMateriaalit = new Text("Materiaalit");
        scenetitleMateriaalit.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        gridMateriaali.add(scenetitleMateriaalit, 0, 0, 2, 1);
        
        Text scenetitleSuunnitelma = new Text("Suunnitelmat");
        scenetitleSuunnitelma.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        gridSuunnitelma.add(scenetitleSuunnitelma, 0, 0, 2, 1);
        
        
        //Tableview asetus
        updateTables();
        gridKayttaja.add(tableKayttajat, 1, 1);
        gridElementti.add(tableElementit,1,1);
        gridMateriaali.add(tableMateriaalit,1,1);
        gridSuunnitelma.add(tableSuunnitelmat,1,1);
        
        //Materiaalin poisto objektit
        
        delMaterialId = new TextField();
        delMaterialId.setPromptText("Id");
        delMaterialId.setMaxWidth(90);
        
        Button delMaterialButton = new Button("Poista");
        delMaterialButton.setMinWidth(90);
        delMaterialButton.setMaxWidth(90);
        
        delMaterialButton.setOnMouseClicked((e) -> {
            int id;
            boolean isInt = true;
            //Tarkistetaan onko annettu id int tyyppinen
            try {
                id = Integer.parseInt(delMaterialId.getText());
            } catch (NumberFormatException x) {
                isInt = false;
                id = -1;
                System.out.println("Annettu id ei ole int tyyppinen");
            }
            
            
            if(isInt){ 
                kayttis.poistaMateriaali(id);
                gridMateriaali.getChildren().remove(tableMateriaalit);
                updateTables();
                gridMateriaali.add(tableMateriaalit,1,1);
                delMaterialId.clear();
            } else {
                System.out.println("Annetut materiaalin id ei kelpaa");
            }
        });
        
        HBox hbMateriaaliDel = new HBox();
        hbMateriaaliDel.setSpacing(10);
        hbMateriaaliDel.getChildren().addAll(delMaterialId,delMaterialButton);
        hbMateriaaliDel.setAlignment(Pos.CENTER_RIGHT);
        
        gridMateriaali.add(hbMateriaaliDel, 1, 3);
        
        //Elementin poisto objektit
        
        delElementId = new TextField();
        delElementId.setPromptText("Id");
        delElementId.setMaxWidth(90);
        
        Button delElementButton = new Button("Poista");
        delElementButton.setMinWidth(90);
        delElementButton.setMaxWidth(90);
        delElementButton.setOnMouseClicked((e) -> {
            int id = -1;
            boolean isInt = true;
            //Tarkistetaan onko annettu id int tyyppinen
            try {
                id = Integer.parseInt(delElementId.getText());
            } catch (NumberFormatException x) {
                isInt = false;
                System.out.println("Annettu id ei ole int tyyppinen");
            }
            
            
            if(isInt){ 
                kayttis.poistaElementti(id);
                gridMateriaali.getChildren().remove(tableElementit);
                updateTables();
                gridElementti.add(tableElementit,1,1);
                delElementId.clear();
            } else {
                System.out.println("Annettu elementti id ei kelpaa");
            }
        });
        
        HBox hbElementtiDel = new HBox();
        hbElementtiDel.setSpacing(10);
        hbElementtiDel.getChildren().addAll(delElementId,delElementButton);
        gridElementti.add(hbElementtiDel, 1, 3);
        
        //Elementin lisäys objektit
        
        addElementName = new TextField();
        addElementName.setPromptText("Nimi");
        addElementName.setMaxWidth(90);
        
        addElementType = new TextField();
        addElementType.setPromptText("Tyyppi");
        addElementType.setMaxWidth(90);
             
        Button addElementButton = new Button("Lisää");
        addElementButton.setMinWidth(90);
        addElementButton.setMaxWidth(90);
        
        Button openElePicButton = new Button("Valitse kuva");
        
        ImageView imgElementView = new ImageView(new Image("file:Default.png"));
        imgElementView.setFitHeight(25);
        imgElementView.setFitWidth(25);
        
        
        HBox hbEleAddPicture = new HBox();
        hbEleAddPicture.setSpacing(10);
        hbEleAddPicture.getChildren().addAll(openElePicButton,imgElementView);
        
        
        openElePicButton.setOnAction(( ActionEvent e) -> {
            valittuElementFile = getElementFile();
            if(valittuElementFile.isFile()){
                hbEleAddPicture.getChildren().remove(1);
                hbEleAddPicture.getChildren().add(updateImage(valittuElementFile));
            }
        });
        gridElementti.add(hbEleAddPicture, 1, 4);
        
        
        addElementButton.setOnMouseClicked((e) -> {
            int type;
            boolean isInt = true;
            byte[] elementKuva = null;
            try {
                elementKuva = Files.readAllBytes(valittuElementFile.toPath());
             } catch (IOException ex) {
                 System.out.println("Et ole valinnut kuvaa elementille");
             }
            //Tarkistetaan onko annettu id int tyyppinen
            try {
                type = Integer.parseInt(addElementType.getText());
            } catch (NumberFormatException x) {
                isInt = false;
                type = -1;
                System.out.println("Annettu id ei ole int tyyppinen");
            }
            if(!addElementName.getText().isEmpty() && isInt && type > 0 && valittuElementFile.isFile() && elementKuva != null){
                kayttis.lisaaElementti(addElementName.getText(),type,valittuElementFile.getName().toLowerCase(),elementKuva);
                saveElementFile();
                gridElementti.getChildren().remove(tableElementit);
                updateTables();
                gridElementti.add(tableElementit,1,1);
                addElementName.clear();
                addElementType.clear();
                hbEleAddPicture.getChildren().remove(1);
                hbEleAddPicture.getChildren().add(new ImageView(new Image("file:Default.png")));
            } else {
                System.out.println("Annettu elementin nimi ei kelpaa");
            }
        });
        
        HBox hbElementti = new HBox();
        hbElementti.setSpacing(10);
        hbElementti.getChildren().addAll(addElementName,addElementType,addElementButton);
        gridElementti.add(hbElementti, 1, 2);
        
        //Kayttajan poisto objektit
        
        removeUserId = new TextField();
        removeUserId.setPromptText("Id");
        removeUserId.setMaxWidth(90);
             
        Button removeUserButton = new Button("Poista käyttäjä");
        removeUserButton.setMinWidth(120);
        removeUserButton.setMaxWidth(150);
        
        removeUserButton.setOnMouseClicked((e) -> {
            int id;
            boolean isInt = true;
            //Tarkistetaan onko annettu id int tyyppinen
            try {
                id = Integer.parseInt(removeUserId.getText());
            } catch (NumberFormatException x) {
                isInt = false;
                id = -1;
                System.out.println("Annettu id ei ole int tyyppinen");
            }
            
            if(!removeUserId.getText().isEmpty() && id != 1 && isInt){
                kayttis.poistaKayttaja(id);
                gridKayttaja.getChildren().remove(tableKayttajat);
                updateTables();
                gridKayttaja.add(tableKayttajat,1,1);
                removeUserId.clear();
            } else {
                System.out.println("Annetun kayttajan nimi ei kelpaa");
            }
        });
        
        HBox hbDelKayttaja = new HBox();
        hbDelKayttaja.setSpacing(10);
        hbDelKayttaja.getChildren().addAll(removeUserId,removeUserButton);
        gridKayttaja.add(hbDelKayttaja, 1, 2);
        
         //Kayttajan oikeuksien muuttamisen objektit
         
        changeUserRightId = new TextField();
        changeUserRightId.setPromptText("Id");
        changeUserRightId.setMaxWidth(90);
             
        
        combo = new ComboBox();
        combo.getItems().addAll("Normaali","Tarkastaja","Ylläpitäjä");
        combo.getSelectionModel().selectFirst();
        
        Button changeUserRightButton = new Button("Vaihda oikeus");
        changeUserRightButton.setMinWidth(120);
        changeUserRightButton.setMaxWidth(150);
        
        changeUserRightButton.setOnMouseClicked((e) -> {
            int id;
            boolean isInt = true;
            //Tarkistetaan onko annettu id int tyyppinen
            try {
                id = Integer.parseInt(changeUserRightId.getText());
            } catch (NumberFormatException x) {
                isInt = false;
                id = -1;
                System.out.println("Annettu id ei ole int tyyppinen");
                return;
            }
            
            if(!changeUserRightId.getText().isEmpty() && isInt){
                kayttis.päivitäOikeudet(id, combo.getSelectionModel().getSelectedIndex()+1);
                gridKayttaja.getChildren().remove(tableKayttajat);
                updateTables();
                gridKayttaja.add(tableKayttajat,1,1);
                changeUserRightId.clear();
                combo.getSelectionModel().selectFirst();
                
            } else {
                System.out.println("Annetun kayttajan nimi ei kelpaa");
            }
        });
        
        HBox hbChangeRight = new HBox();
        hbChangeRight.setSpacing(10);
        hbChangeRight.getChildren().addAll(changeUserRightId,combo,changeUserRightButton);
        gridKayttaja.add(hbChangeRight, 1, 3);
        
        
        //Materiaalin kuvan asettaminen
        Button openButton = new Button("Valitse kuva");
        
        Image img = new Image("file:Default.png");
        ImageView imgView = new ImageView(img);
        imgView.setFitHeight(25);
        imgView.setFitWidth(25);
        
        
        HBox hbAddPicture = new HBox();
        hbAddPicture.setSpacing(10);
        hbAddPicture.getChildren().addAll(openButton,imgView);
        
        
        openButton.setOnAction(( ActionEvent e) -> {
            valittuMaterialFile = getMaterialFile();
            if(valittuMaterialFile.isFile()){
                hbAddPicture.getChildren().remove(1);
                hbAddPicture.getChildren().add(updateImage(valittuMaterialFile));
            }
        });
        gridMateriaali.add(hbAddPicture, 1, 4);
        
        
        //Tabien asettelut
        kayttajaTab.setContent(gridKayttaja);
        elementtiTab.setContent(gridElementti);
        materiaaliTab.setContent(gridMateriaali);
        suunnitelmaTab.setContent(gridSuunnitelma);
        
        //Materiaalin lisäys objektit
        
        addMaterialName = new TextField();
        addMaterialName.setPromptText("Nimi");
        addMaterialName.setMaxWidth(90);
        
        addMaterialPrice = new TextField();
        addMaterialPrice.setPromptText("Hinta");
        addMaterialPrice.setMaxWidth(90);
        
        
        addMaterialEType = new TextField();
        addMaterialEType.setPromptText("eTyyppi");
        addMaterialEType.setMaxWidth(90);
        
        Button addMaterialButton = new Button("Lisää");
        addMaterialButton.setMinWidth(90);
        addMaterialButton.setMaxWidth(90);
        addMaterialButton.setOnMouseClicked((MouseEvent e) -> {
            int eTyyppi;
            boolean isInt = true;
            //Tarkistetaan onko annettu elementti id int tyyppinen
            try {
                eTyyppi = Integer.parseInt(addMaterialEType.getText());
            } catch (NumberFormatException x) {
                isInt = false;
                eTyyppi = -1;
                System.out.println("Annettu elementti tyyppi ei ole int tyyppinen");
            }
            
            //Tarkistetaan että annettu luku on double tyyppinen
            double hinta;
            boolean isDouble = true;
            byte[] kuva = null;
            try {
                hinta = Double.parseDouble(addMaterialPrice.getText());
            } catch (NumberFormatException x) {
                isDouble = false;
                hinta = -1;
                System.out.println("Annettu hinta ei ole double tyyppinen");
            }
            
            try {
                kuva = Files.readAllBytes(valittuMaterialFile.toPath());
             } catch (IOException ex) {
                 System.out.println("Et ole valinnut kuvaa materiaalille");
             }
            if(!addMaterialName.getText().isEmpty() && !addMaterialPrice.getText().isEmpty() && !addMaterialEType.getText().isEmpty()
                && isDouble && hinta >=0 && valittuMaterialFile.isFile() && isInt && eTyyppi >0 && kuva != null){
                
                kayttis.lisaaMateriaali(addMaterialName.getText(), hinta, valittuMaterialFile.getName().toLowerCase(), eTyyppi,kuva);
                saveMaterialFile();
                gridMateriaali.getChildren().remove(tableMateriaalit);
                updateTables();
                gridMateriaali.add(tableMateriaalit,1,1);
                addMaterialName.clear();
                addMaterialPrice.clear();
                addMaterialEType.clear();
                hbAddPicture.getChildren().remove(1);
                hbAddPicture.getChildren().add(new ImageView(new Image("Default.png")));
            } else {
                System.out.println("Annetut materiaalin arvot ei kelpaa");
                if(!valittuMaterialFile.isFile()){
                    System.out.println("Et ole valinnut kuvaa materiaalille");
                }
            }
        });
        
        HBox hbMateriaaliAdd = new HBox();
        hbMateriaaliAdd.setSpacing(10);
        hbMateriaaliAdd.getChildren().addAll(addMaterialName,addMaterialPrice,addMaterialEType,addMaterialButton);
        gridMateriaali.add(hbMateriaaliAdd, 1, 2);
        
        //Suunnitelman poisto objektit
        
        removePlanId = new TextField();
        removePlanId.setPromptText("Id");
        removePlanId.setMaxWidth(90);
             
        Button removePlanButton = new Button("Poista suunnitelma");
        removePlanButton.setMinWidth(120);
        removePlanButton.setMaxWidth(150);
        
        removePlanButton.setOnMouseClicked((e) -> {
            int id;
            boolean isInt = true;
            //Tarkistetaan onko annettu id int tyyppinen
            try {
                id = Integer.parseInt(removePlanId.getText());
            } catch (NumberFormatException x) {
                isInt = false;
                id = -1;
                System.out.println("Annettu id ei ole int tyyppinen");
            }
            
            if(!removePlanId.getText().isEmpty() && isInt){
                kayttis.poistaSuunnitelma(id);
                gridSuunnitelma.getChildren().remove(tableSuunnitelmat);
                updateTables();
                gridSuunnitelma.add(tableSuunnitelmat,1,1);
                removePlanId.clear();
            } else {
                System.out.println("Annetun suunnitelman id ei kelpaa");
            }
        });
        
        HBox hbDelPlan = new HBox();
        hbDelPlan.setSpacing(10);
        hbDelPlan.getChildren().addAll(removePlanId,removePlanButton);
        gridSuunnitelma.add(hbDelPlan, 1, 2);

        stage.setOnCloseRequest(event -> {
                
        });
        
        stage.setOnShowing((WindowEvent event) -> {
            defaultOnLoadActions();
            gridElementti.getChildren().remove(tableElementit);
            gridElementti.add(kayttis.getElementtiTable(),1,1);
            
            gridKayttaja.getChildren().remove(tableKayttajat);
            gridKayttaja.add(kayttis.getKayttajaTable(),1,1);
         
            gridMateriaali.getChildren().remove(tableMateriaalit);
            gridMateriaali.add(kayttis.getMateriaaliTable(),1,1);
            
            gridSuunnitelma.getChildren().remove(tableSuunnitelmat);
            gridSuunnitelma.add(kayttis.getSuunnitelmaTable(),1,1);
        });
       borderPane.setCenter(tabPane);
       stage.setScene(scene); 
    }
    private void updateTables() {
        tableKayttajat = kayttis.getKayttajaTable();
        tableElementit = kayttis.getElementtiTable();
        tableMateriaalit = kayttis.getMateriaaliTable();
        tableSuunnitelmat = kayttis.getSuunnitelmaTable();
    }
    private File getMaterialFile (){
        File file = fileChooser.showOpenDialog(stage);
        if(file.isFile()){
            valittuMaterialFile = file;
            fileChooser.setInitialDirectory(file.getParentFile());
            System.out.println(file+" valittuna");
        } 
        return file;
    }
    private File getElementFile (){
        File file = fileChooser.showOpenDialog(stage);
        if(file.isFile()){
            valittuElementFile = file;
            fileChooser.setInitialDirectory(file.getParentFile());
            System.out.println(file+" valittuna");
        } 
        return file;
    }
    private boolean saveMaterialFile (){
        File file = valittuMaterialFile;
        File outputFile = new File(file.getName());
        try {
            Files.copy(file.toPath(), outputFile.toPath() ,StandardCopyOption.REPLACE_EXISTING);
            System.out.println(file.getName()+" siirretty paikkaan "+file.getPath());
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    private boolean saveElementFile (){
        File file = valittuElementFile;
        File outputFile = new File(file.getName());
        try {
            Files.copy(file.toPath(), outputFile.toPath() ,StandardCopyOption.REPLACE_EXISTING);
            System.out.println(file.getName()+" siirretty paikkaan "+file.getPath());
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    private ImageView updateImage (File file) {
        Image img = new Image(file.toURI().toString());
        ImageView imgView = new ImageView(img);
        imgView.setFitHeight(25);
        imgView.setFitWidth(25);
        return imgView;
    }

    @Override
    void backButtonAction() {
        stage.hide();
        kayttis.showEtusivuStage();
    }
    @Override
    protected void resetStage(){
        removePlanId.clear();
        addMaterialEType.clear();
        addMaterialPrice.clear();
        addElementName.clear();
        addElementType.clear();
        removeUserId.clear();
        changeUserRightId.clear();
        addMaterialName.clear();
        delMaterialId.clear();
        delElementId.clear();
        combo.getSelectionModel().selectFirst();
    }
}

