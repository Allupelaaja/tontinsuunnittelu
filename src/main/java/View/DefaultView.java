/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.util.Optional;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import static javafx.geometry.Orientation.HORIZONTAL;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Separator;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author JukkaPC
 */
public abstract class DefaultView {
    protected Stage stage;
    protected Kayttoliittyma kayttis;
    protected Scene scene;
    protected BorderPane borderPane;
    protected Button backBtn,logoutBtn,settingsBtn,yllapitoBtn;
    protected ToolBar vaakaToolBar;
    protected Pane spacer;
    protected String alertText;
    protected Text username = new Text();
    public DefaultView(Kayttoliittyma kayttoliittyma, double width, double height) {
        stage = new Stage();
        borderPane = new BorderPane();
        kayttis = kayttoliittyma;
        
        stage.setWidth(width);
        stage.setHeight(height);
        stage.setMinWidth(width);
        stage.setMinHeight(height);
        
        alertText =  "Haluatko varmasti sulkea ohjelman?";
        //Borderpane
//        BackgroundImage myBI= new BackgroundImage(new Image("file:src/main/resources/hiekkatausta.png"),
//        BackgroundRepeat.ROUND, BackgroundRepeat.ROUND, BackgroundPosition.CENTER,BackgroundSize.DEFAULT);
//        
//        borderPane.setBackground(new Background(myBI));
        scene = new Scene(borderPane, stage.getWidth(), stage.getHeight());
                
       ImageView iv = new ImageView(new Image("cog1.png"));
       iv.setFitHeight(25);
       iv.setFitWidth(25);
       
       ImageView iv2 = new ImageView(new Image("nuoli.png"));
       iv2.setFitHeight(25);
       iv2.setFitWidth(25);
       
       ImageView ivLogout = new ImageView(new Image("logoutkuva.png"));
       ivLogout.setFitHeight(25);
       ivLogout.setFitWidth(25);        
        
        //Takaisin etusivulle -nappi
        
        backBtn = new Button("", iv2);
        backBtn.setTooltip(new Tooltip("Etusivulle"));
        
        //Uloskirjautumisnappi
        
        logoutBtn = new Button("", ivLogout);
        logoutBtn.setTooltip(new Tooltip("Kirjaudu ulos"));
        
        //Asetusnappi
        
        settingsBtn = new Button("", iv);
        settingsBtn.setTooltip(new Tooltip("Asetukset"));
        
        //
        //NAPPIEN ACTIONIT
        //
        
        
        yllapitoBtn = new Button("Ylläpito");
        //Ylläpitonapin action
        yllapitoBtn.setOnMouseClicked((MouseEvent e) -> {
            kayttis.showYllapitoStage();
            
        });
        
        //Asetusnapin action
        settingsBtn.setOnAction((ActionEvent e) -> {
            kayttis.showAsetusStage();
        });
        
        //Uloskirjautumisnapin action
        logoutBtn.setOnAction((ActionEvent e) -> {
             logoutButtonAction();
        });
        
        //Etusivulle-napin action
        backBtn.setOnAction((ActionEvent e) -> {
             backButtonAction();
        });
        
        //Poistuu fullscreenista escape-napista
        scene.addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent t) -> {
            if(t.getCode()==KeyCode.ESCAPE) {
                kayttis.vaihdaFullscreen(false);
            }
        });
        //Ohjelman ikkunan ruksin funktio
        stage.setOnCloseRequest(event -> {
            //confirmation ikkuna
            Alert closeConfirmation = new Alert(
                    Alert.AlertType.CONFIRMATION,
                    alertText
            );
            Button exitButton = (Button) closeConfirmation.getDialogPane().lookupButton(
                    ButtonType.OK
            );
            exitButton.setText("Poistu");
            closeConfirmation.setHeaderText("Poistumisen vahvistus");
            closeConfirmation.initModality(Modality.WINDOW_MODAL);
            closeConfirmation.initOwner(stage);

            closeConfirmation.setX(stage.getWidth()/2-closeConfirmation.getWidth()/2);
            closeConfirmation.setY(stage.getHeight()/2-closeConfirmation.getHeight()/2);

            Optional<ButtonType> closeResponse = closeConfirmation.showAndWait();
            if (!ButtonType.OK.equals(closeResponse.get())) {
                event.consume();
            } else {
                alertAction();
            }
        });
        
        
        spacer = new Pane();
        HBox.setHgrow(spacer, Priority.SOMETIMES);
        
        username.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        
        
        vaakaToolBar = new ToolBar(
                settingsBtn,
                new Separator(HORIZONTAL),
                backBtn,
                new Separator(HORIZONTAL),
                spacer,
                yllapitoBtn,
                new Separator(HORIZONTAL),
                username,
                logoutBtn
                
        );
        stage.setOnShowing((WindowEvent event) -> {
            defaultOnLoadActions();
       });
        stage.setOnHidden((WindowEvent event) -> {
            resetStage();
       });
        
        borderPane.setTop(vaakaToolBar);
        stage.setScene(scene);
    }
    
    public Stage getStage() {
        return this.stage;
    }
    public void setStage(Stage stage) {
        this.stage = stage;
    }
    public Scene getScene() {
        return scene;
    }
    public void setScene(Scene scene) {
        stage.setScene(scene);
    }
    protected void alertAction(){
        Platform.exit();
        System.exit(0);
    }
    protected void defaultOnLoadActions() {
        username.setText(kayttis.getKirjautunutKayttaja());
        yllapitoBtn.setDisable(kayttis.getKayttajaOikeus() != 3);
    }
    protected void logoutButtonAction(){
        stage.hide();
        kayttis.hideEtusivuStage();
        kayttis.showKirjauduStage();
    }
    abstract void backButtonAction();
    protected void resetStage (){};
    
    
            
}
