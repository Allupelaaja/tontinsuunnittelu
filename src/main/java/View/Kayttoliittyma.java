/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.Ohjain;
import Model.Tonttisuunnitelma;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 *
 * @author Allupelaaja
 */
public class Kayttoliittyma extends Application {
    Kirjautuminen kirjaudu;
    Luokayttaja luo;
    Etusivu etu;
    Ohjain ohjain;
    Asetukset asetukset;
    Galleria galleria;
    Tonttinakyma tonttiNakyma;
    Tontinmaarittely maarittely;
    Budjetinmaarittely budjetti;
    Kayttajasivu kayttaja;
    Yllapito yllapito;
    SuunnitelmaValikko suunnitelma;
    String kirjautunutKayttaja = "Tuntematon";
    ResourceBundle languages;
    Properties properties;
    Locale currentLocale;
    /**
     *
     */
    public Kayttoliittyma() {
        ohjain = new Ohjain(this);
        //tonttiNakyma = new Tonttinakyma(this);
        kirjaudu = new Kirjautuminen(this);
        luo = new Luokayttaja(this);
        etu = new Etusivu(this);
        asetukset = new Asetukset(this);
        galleria = new Galleria(this);
        maarittely = new Tontinmaarittely(this);
        budjetti = new Budjetinmaarittely(this);
        kayttaja = new Kayttajasivu(this);
        yllapito = new Yllapito(this);
        suunnitelma = new SuunnitelmaValikko(this);
        
        /* Ei toimi vielä, ei löydä resourcebundlea
        properties = new Properties();
        
        String language = "";
        String country = "";
        
        //Ladataan asetustiedosto
        try {
            properties.load(new FileInputStream("Settings.properties"));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Tiedostoa ei löytynyt");
        }
        
        //Kokeillaan hakea current kieli ja maa Settings.properties tiedostosta
        try {
            language = properties.getProperty("currentLanguage");
            country = properties.getProperty("currentCountry");
            System.out.println(language + " " + country);
            if (language.equals("default") && country.equals("default")) {
                System.out.println("Default kieli asetettu");
                //Jos kielitietoja ei löydetä, käytetään defaulteja (Suomi)
                //getBundle ilman localea hakee automaattisesti defaultin (MessagesBundle)
                languages = ResourceBundle.getBundle("MessagesBundle");
            } else {
                //Jos löydetään kielitiedot tiedostosta, 
                //asetetaan kieleksi ja maaksi kyseiset tiedot
                language = properties.getProperty("currentLanguage");
                country = properties.getProperty("currentCountry");
                currentLocale = new Locale(language, country);
                //haetaan esim MessagesBundle_en_GB tiedosto localen tietojen avulla
                languages = ResourceBundle.getBundle("MessagesBundle", currentLocale);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Kyseistä riviä ei löytynyt tiedostosta");
        }
        */
        
        
        
    }

    public Tonttinakyma getTonttiNakyma() {
        return tonttiNakyma;
    }
    /**
     *
     * @return
     */
    public String getKirjautunutKayttaja() {
        return kirjautunutKayttaja;
    }

    /**
     *
     * @param kirjautunutKayttaja
     */
    public void setKirjautunutKayttaja(String kirjautunutKayttaja) {
        this.kirjautunutKayttaja = kirjautunutKayttaja;
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) {

    }
    
    public void showSuunitelmaValikkoStage() {
        suunnitelma.getStage().show();
    }
    
    public void showBudjettiStage(Tonttisuunnitelma tontti) {
        budjetti.setTontti(tontti);
        budjetti.getStage().show();
    }
    
    public void hideEtusivuStage() {
        etu.getStage().hide();
    }
    public void showYllapitoStage() {
        yllapito.getStage().show();
    }
    
    
    /**
     *
     */
    public void showKirjauduStage() {
        kirjautunutKayttaja = "Tuntematon";
        kirjaudu.getStage().show();
    }
    
    
    /**
     *
     */
    public void showEtusivuStage() {
        etu.getStage().show();
    }
    
    
    /**
     *
     */
    public void showLuoStage() {
        luo.getStage().show();
    }
    
    
    /**
     *
     */
    public void showAsetusStage() {
        asetukset.getStage().show();
    }  
    /**
     *
     */
    public void showGalleriaStage() {
        galleria.getStage().show();
    }
    
    /**
     *
     * @param tontti
     */
    public void newTonttiNakyma(Tonttisuunnitelma tontti) {
        ohjain.setTontti(tontti);
        tonttiNakyma = new Tonttinakyma(this,tontti);
//        if(!kirjautunutKayttaja.equals("Tuntematon") && tontti.getOmistaja() != null){
//            setOmistaja(kirjautunutKayttaja);
//        }
    }
    public void showTonttinakymaStage(Tonttisuunnitelma tontti) {
        
        newTonttiNakyma(tontti);
        tonttiNakyma.getStage().show();
    }
    
    /**
     *
     */
    public void showMaarittelyStage() {
        maarittely.getStage().show();
    }
    
    /**
     *
     */
    public void showKayttajaStage() {
        kayttaja.getStage().show();
    }

    /**
     * Vaihtaa ohjelman fullscreen-tilaan tai siitä pois
     * @param vaihto True tai false; vaihdetaanko fullscreen päälle vai pois
     */
    
    public void vaihdaFullscreen(boolean vaihto) {
        
        asetukset.getStage().setFullScreen(vaihto);
        etu.getStage().setFullScreen(vaihto);
        galleria.getStage().setFullScreen(vaihto);
        //tonttiNakyma.getStage().setFullScreen(vaihto);
        maarittely.getStage().setFullScreen(vaihto);
        kayttaja.getStage().setFullScreen(vaihto);
        suunnitelma.getStage().setFullScreen(vaihto);
        kirjaudu.getStage().setFullScreen(vaihto);
        
        kirjaudu.getStage().setFullScreenExitHint("");
        etu.getStage().setFullScreenExitHint("");
        galleria.getStage().setFullScreenExitHint("");
        //tonttiNakyma.getStage().setFullScreenExitHint("");
        maarittely.getStage().setFullScreenExitHint("");
        kayttaja.getStage().setFullScreenExitHint("");
        suunnitelma.getStage().setFullScreenExitHint("");
        if (vaihto == false) {
            asetukset.emptyFullscreenbox();
        }
    }
    
    /**
     * Yrittää kirjautua sisään annetuilla tiedoilla
     * @param nimi Sisäänkirjautuvan käyttäjän käyttäjänimi
     * @param salasana Sisäänkirjautuvan käyttäjän salasana
     * @return Palauttaa onnistuiko
     */
    public boolean kirjaudu(String nimi, String salasana){
        if(ohjain.kirjaudu(nimi, salasana)){
            kirjautunutKayttaja = nimi;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Yrittää luoda käyttäjän annetuilla tiedoilla
     * @param nimi Luotavan käyttäjän käyttäjänimi
     * @param salasana Luotavan käyttäjän salasana
     * @return Palauttaa onnistuiko
     */
    public boolean luoKayttaja(String nimi, String salasana) {
        return ohjain.luoKayttaja(nimi, salasana);
    }

    /**
     * Yrittää vaihtaa salasanan vanhasta uuteen
     * @param vanha Vanha salasana
     * @param uusi Uusi salasana
     * @return Palauttaa onnistuiko
     */
    public boolean vaihdaSalasana(String vanha, String uusi){
        if(kirjautunutKayttaja.equals("admin")){
            return false;
        } 
        return ohjain.vaihdaSalasana(kirjautunutKayttaja ,vanha,uusi);
    }

    /**
     * Yrittää vaihtaa käyttäjänimen vanhasta uuteen
     * @param uusi Uusi käyttäjänimi
     * @return Palauttaa onnistuiko
     */
    public boolean vaihdaNimi(String uusi){
        if(kirjautunutKayttaja.equals("admin")){
            return false;
        } 
        return ohjain.vaihdaNimi(kirjautunutKayttaja,uusi);
    }
    
    public TableView getKayttajaTable(){
        return ohjain.getKayttajaTable();
    }
    
    public TableView getElementtiTable(){
        return ohjain.getElementtiTable();
    }
    public TableView getMateriaaliTable(){
        return ohjain.getMateriaaliTable();
    }
    
    public boolean lisaaElementti(String nimi, int tyyppi, String kuvanNimi,byte[]kuva){
        return ohjain.lisaaElementti(nimi,tyyppi,kuvanNimi, kuva);
    }
    public boolean lisaaMateriaali (String nimi, double hinta, String kuvanNimi, int eTyyppi, byte[] kuva){
        return ohjain.lisaaMateriaali(nimi, hinta, kuvanNimi,eTyyppi,kuva);
    }
    public boolean poistaKayttaja(int id){
        return ohjain.poistaKayttaja(id);
    }
    public boolean poistaMateriaali(int id){
        return ohjain.poistaMateriaali(id);
    }
    public boolean poistaElementti(int id){
        return ohjain.poistaElementti(id);
    }
    public Scene getSuunnitelmaScene() {
        return suunnitelma.getScene();
    }
    public void setEtusivuScene(Scene scene) {
        etu.setScene(scene);
    }
    public Scene getEtusivuScene() {
        return etu.getScene();
    }
    
    public Scene getGalleriaScene() {
        return galleria.getScene();
    }
    
    public ArrayList<FlowPane> getMateriaaliPanet() {
         return ohjain.getMateriaaliPanet();
    }
    public void lisaaRuutuun(String x, String y, String alusta, String elementti) {
        ohjain.lisaaRuutuun(x, y, alusta, elementti);
    }
    
    public void vaihdaRuutu(String x, String y, String alusta, String elementti) {
        ohjain.vaihdaRuutu(x, y, alusta, elementti);
    }
    
    public String[] haeRuudunKomponentit(int x, int y) {
        return ohjain.haeRuudunKomponentit(x, y);
    }
    public ArrayList<Button> getElementtiButtons() {
        return ohjain.getElementtiButtons();
    }
    public String getMaterialPicName(String materialName){
        return ohjain.getMaterialPicName(materialName);
    }
    public List<String> getAlustat() {
        return ohjain.getAlustat();
    }
    public boolean tallennaSuunnitelma(Tonttisuunnitelma suunnitelma){
        return ohjain.tallennaSuunnitelma(suunnitelma);
    }
    public Tonttisuunnitelma getSuunnitelma(String suunnitelmaNimi) {
        return ohjain.getSuunnitelma(kirjautunutKayttaja, suunnitelmaNimi);
    }
    public Tonttisuunnitelma getSuunnitelma(String suunnitelmaNimi,String omistaja) {
        return ohjain.getSuunnitelma(omistaja, suunnitelmaNimi);
    }
    public boolean setOmistaja(String omistaja){
        return ohjain.setOmistaja(omistaja);
    }
    public void setTontti(Tonttisuunnitelma ts){
        ohjain.setTontti(ts);
    }
    public boolean päivitäOikeudet (int id, int oikeus) {
        return ohjain.päivitäOikeudet(id, oikeus);
    }
    public TableView getSuunnitelmaTable() {
        return ohjain.getSuunnitelmaTable();
    }
    public boolean poistaSuunnitelma(int id) {
        return ohjain.poistaSuunnitelma(id);
    }
//    public boolean poistaSuunnitelma(Object o) {
//        return ohjain.poistaSuunnitelma(o);
//    }
    public void tallennaKuvat (){
        ohjain.tallennaKuvat();
    }
    public List<Tonttisuunnitelma> getSuunnitelmaTaulu(){
        return ohjain.getSuunnitelmaTaulu();
    }
    public int getKayttajaOikeus(){
        return ohjain.getKayttajaOikeus(kirjautunutKayttaja);
    }
//    public ArrayList<Button> getSuunnitelmaButtons () {
//        return ohjain.getSuunnitelmaButtons(kirjautunutKayttaja);
//    }
    public ArrayList<GridPane> getSuunnitelmaGrids () {
        return ohjain.getSuunnitelmaGrids(kirjautunutKayttaja);
    }
    public boolean poistaSuunnitelma(String suunnitelmaNimi) {
         return ohjain.poistaSuunnitelma(suunnitelmaNimi,kirjautunutKayttaja);
    }
    public void jaaSuunnitelma(String suunnitelmaNimi){
        ohjain.jaaSuunnitelma(getSuunnitelma(suunnitelmaNimi));
        
    }
    public ArrayList<GridPane> getJaetutSuunnitelmaGrids () {
        return ohjain.getJaetutSuunnitelmaGrids();
    }
}
