/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.WindowEvent;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Border;



/**
 *
 * @author Allupelaaja
 */
public class Kirjautuminen extends DefaultView { 
    static final double LEVEYS = 350;
    static final double KORKEUS = 350;
    private PasswordField pwBox;
    private TextField userTextField;
    private  Text actiontarget;
    /**
     *
     * @param kayttoliittyma
     */
    public Kirjautuminen(Kayttoliittyma kayttoliittyma) {
        super(kayttoliittyma,LEVEYS,KORKEUS);
        backBtn.setDisable(true);
        logoutBtn.setDisable(true);
        vaakaToolBar.setVisible(false);
        vaakaToolBar.setManaged(false);
        //Stage
        stage.setTitle("Kirjautuminen");
        
        //Grid
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10); //Leveys
        grid.setVgap(10); //Korkeus
        grid.setPadding(new Insets(25, 25, 25, 25));
        
        borderPane.setCenter(grid);
        //Tekstikentät
        Text scenetitle = new Text("Tontinsuunnittelu");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);
        
        Label userName = new Label("Käyttäjätunnus:");
        grid.add(userName, 0, 1);
        
        userTextField = new TextField();
        userTextField.setId("userTextField");
        grid.add(userTextField, 1, 1);
        
        Label pw = new Label("Salasana:");
        grid.add(pw, 0, 2);
        
        
        pwBox = new PasswordField();
        pwBox.setId("pwBox");
        grid.add(pwBox, 1, 2);
        
        
        //Messaget napeille
        
       
        actiontarget = new Text();
        grid.add(actiontarget, 1, 5);
        
        //
        // NAPIT
        //
        
        //Sisäänkirjautumisnappi
        Button loginBtn = new Button("Kirjaudu sisään");
        loginBtn.setId("loginBtn");
        HBox loginHbBtn = new HBox(10);
        loginHbBtn.setAlignment(Pos.BOTTOM_CENTER);
        loginHbBtn.getChildren().add(loginBtn);
        grid.add(loginHbBtn, 1, 4);
        
        //Nappi tuntemattomalle
        
        Button altLoginBtn = new Button("Kirjaudu sisään tuntemattomana");
        HBox altLoginhbBtn = new HBox(10);
        altLoginhbBtn.setAlignment(Pos.BOTTOM_CENTER);
        altLoginhbBtn.getChildren().add(altLoginBtn);
        grid.add(altLoginhbBtn, 1, 7);
        
        //Nappi uudelle käyttäjälle
        
        Button createLoginBtn = new Button("Luo uusi käyttäjä");
        HBox createLoginHbBtn = new HBox(10);
        createLoginHbBtn.setAlignment(Pos.BOTTOM_CENTER);
        createLoginHbBtn.getChildren().add(createLoginBtn);
        grid.add(createLoginHbBtn, 1, 8);
        
        //
        //NAPPIEN ACTIONIT
        //
        
        //Kirjautumisnapin action
        loginBtn.setOnAction((ActionEvent e) -> {
            String kayttajanimi = userTextField.getText();
            String salasana = pwBox.getText();
            
            if (!kayttajanimi.isEmpty() && !salasana.isEmpty() && kayttis.kirjaudu(kayttajanimi, salasana)) {
                    stage.hide();
                    kayttis.showEtusivuStage();
                    username.setText(kayttajanimi);
                } else {
                    actiontarget.setFill(Color.FIREBRICK);
                    actiontarget.setText("Kirjautuminen epäonnistui");
                }
        });
        
        //Tuntemattoman kirjautumisnapin action
        altLoginBtn.setOnAction((ActionEvent e) -> {
            stage.hide();
            kayttis.showEtusivuStage();
        });
        
        //Käyttäjänluontibuttonin action
        createLoginBtn.setOnAction((ActionEvent e) -> {
            stage.hide();
            kayttis.showLuoStage();
        });
        
        //Sulkee ohjelman myös taustalta kun painaa punaista ruksia
        stage.setOnCloseRequest(event -> {
                alertAction();
        });
        
        //Kirjautuu sisään enter-napista
        scene.addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent t) -> {
            if(t.getCode()==KeyCode.ENTER) {
                loginBtn.fire();
            }
        });
        stage.setOnShowing((WindowEvent event) -> {
            defaultOnLoadActions();
            userTextField.requestFocus();
        });
        
        stage.show();
    }

    @Override
    void backButtonAction() {
        System.out.println("Et voi mennä takaisin");
    }
    @Override
    protected void resetStage() {
        pwBox.clear();
        userTextField.clear();
        actiontarget.setText("");
    }
}
