/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.util.ArrayList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

/**
 *
 * @author Jukka
 */
public class SuunnitelmaValikko extends DefaultView {
    private static final double WIDTH = 500, HEIGHT = 500;
    private FlowPane flowPane;
    public SuunnitelmaValikko(Kayttoliittyma kayttoliittyma) {
        super(kayttoliittyma, WIDTH, HEIGHT);
        stage.setTitle("Valitse suunnitelma");
        
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10); //Leveys
        grid.setVgap(10); //Korkeus
        grid.setPadding(new Insets(25, 25, 25, 25));
        
        borderPane.setCenter(grid);
        //Tekstikentät
        Text scenetitle = new Text("Valitse suunnitelma");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        
        HBox titleHb = new HBox();
        titleHb.getChildren().add(scenetitle);
        titleHb.setAlignment(Pos.CENTER);
        grid.add(titleHb, 0, 0, 2, 1);
        
        
        flowPane = new FlowPane();
        flowPane.setPadding(new Insets(10, 10, 10,10));
        flowPane.setVgap(4);
        flowPane.setHgap(4);
        
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(flowPane);
        
        
        //toolbarScroller.setMaxHeight(55);
//        scrollPane.setFitToWidth(true);
//        scrollPane.setFitToHeight(true);
        grid.add(scrollPane, 0, 1);
        stage.setOnShowing(event -> { 
            defaultOnLoadActions();
            updateFlowPane();
        });
        
        
        stage.setOnCloseRequest(event -> {
            backButtonAction();
        });     
    }
    private void updateFlowPane() {
        flowPane.getChildren().clear();
        ArrayList<GridPane> grids = kayttis.getSuunnitelmaGrids();
            for(GridPane g : grids){
                //      0       1      2        3       4
                //g : [HBox,ImageView,openBtn,delBtn,shareBtn]
                String nimi = ((Text)((HBox) g.getChildren().get(0)).getChildren().get(0)).getText();
                    Button openBtn = (Button) g.getChildren().get(2);
                    Button delBtn = (Button) g.getChildren().get(3);
                    Button shareBtn = (Button) g.getChildren().get(4);
                
                //Open button
                openBtn.setOnMouseClicked(e -> {
//                   String omistaja = ((Text)((HBox) g.getChildren().get(5)).getChildren().get(0)).getText();
                   kayttis.showTonttinakymaStage(kayttis.getSuunnitelma(nimi));
                   stage.hide();
                });
                
                //Delete button
                delBtn.setOnMouseClicked(e -> {
                    kayttis.poistaSuunnitelma(nimi);
                    updateFlowPane();
                });
                
                //Share button
                shareBtn.setOnMouseClicked(e -> {
                    kayttis.jaaSuunnitelma(nimi);
                    updateFlowPane();
                });
                flowPane.getChildren().add(g);
            }
    }
    @Override
    void backButtonAction() {
        stage.hide();
//        kayttis.showEtusivuStage();
//        kayttis.setEtusivuScene(kayttis.getEtusivuScene());
    }
    @Override
    protected void resetStage () {
        flowPane.getChildren().clear();
    }
}
