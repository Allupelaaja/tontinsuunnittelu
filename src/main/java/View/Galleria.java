/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.util.ArrayList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.WindowEvent;


/**
 *
 * @author Allupelaaja
 */

public class Galleria extends DefaultView{
    /**
     *
     * @param kayttoliittyma
     */
     FlowPane flowPane;
     static final double LEVEYS = 500;
     static final double KORKEUS = 500;
    
    public Galleria(Kayttoliittyma kayttoliittyma) {
        super(kayttoliittyma, LEVEYS, KORKEUS);
        
        //Stage
        stage.setTitle("Galleria");
        
        BorderPane innerBP = new BorderPane();
        borderPane.setCenter(innerBP);
        
        //Tekstikentät
        Text scenetitle = new Text("Galleria");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        
        HBox titleHb = new HBox();
        titleHb.getChildren().add(scenetitle);
        titleHb.setAlignment(Pos.CENTER);

        innerBP.setTop(titleHb);
        
        flowPane = new FlowPane();
        flowPane.setPadding(new Insets(10, 10, 10,10));
        flowPane.setVgap(4);
        flowPane.setHgap(4);
        
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(flowPane);
        scrollPane.setFitToWidth(true);
        scrollPane.setFitToHeight(true);
        
        innerBP.setCenter(scrollPane);
        
        stage.setOnShowing((WindowEvent event) -> {
            defaultOnLoadActions();
            updateFlowPane();
        });
        stage.setOnCloseRequest(event -> {
                
        });
    }
    private void updateFlowPane() {
        flowPane.getChildren().clear();
        ArrayList<GridPane> grids = kayttis.getJaetutSuunnitelmaGrids();
            for(GridPane g : grids){
                g.setPadding(new Insets(10, 10, 10, 10));
                //      0       1      2        3       4
                //g : [HBox,ImageView,openBtn,delBtn,shareBtn]
                String nimi = ((Text)((HBox) g.getChildren().get(0)).getChildren().get(0)).getText();
                    Button openBtn = (Button) g.getChildren().get(2);
                //Open button
                openBtn.setOnMouseClicked(e -> {
                   String omistaja = ((Text)((HBox) g.getChildren().get(3)).getChildren().get(0)).getText();
                   kayttis.showTonttinakymaStage(kayttis.getSuunnitelma(nimi,omistaja));
                   stage.hide();
                });
                flowPane.getChildren().add(g);
            }
    }
    @Override
    void backButtonAction() {
        stage.hide();
        kayttis.showEtusivuStage();
    }
}
