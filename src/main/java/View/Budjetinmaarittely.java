/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.Tonttisuunnitelma;
import java.util.Optional;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.WindowEvent;

/**
 *
 * @author Matti
 */
public class Budjetinmaarittely extends DefaultView{
    private Tonttisuunnitelma tontti;
    static final double LEVEYS = 350;
    static final double KORKEUS = 300;
    private TextField budgetField;
    public Budjetinmaarittely (Kayttoliittyma kayttoliittyma) {
        super(kayttoliittyma, LEVEYS, KORKEUS);
        stage.setTitle("Aseta budjetti");
        
        yllapitoBtn.setVisible(false);
        yllapitoBtn.setManaged(false);
        
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10); //Leveys
        grid.setVgap(10); //Korkeus
        grid.setPadding(new Insets(25, 25, 25, 25));

        borderPane.setCenter(grid);
        
        Text scenetitle = new Text();
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);
        
        Label budget = new Label("Budjetti(€)");
        grid.add(budget, 0, 1);
        
        
        budgetField = new TextField();
        grid.add(budgetField, 1, 1);
        
        Button acceptBtn = new Button("Hyväksy");
        HBox acceptHbBtn = new HBox(10);
        acceptHbBtn.setAlignment(Pos.CENTER);
        acceptHbBtn.getChildren().add(acceptBtn);
        grid.add(acceptHbBtn, 0, 3);
        
        acceptBtn.setOnAction((ActionEvent e) -> {
            try {
                String budget1 = budgetField.getText();
                double uusiBudjetti = Double.parseDouble(budget1);
                System.out.println("Vanha budjetti: "+tontti.getBudjetti());
                tontti.setBudjetti(uusiBudjetti);
                System.out.println(tontti.getBudjetti());
                kayttis.setTontti(tontti);
                stage.hide();
            }catch(NumberFormatException ie) {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Virhe luvun lukemisesa");
                alert.setHeaderText(null);
                alert.setContentText("Syötteen tulee olla liukuluku");
                alert.showAndWait();
                ie.printStackTrace();
            }
        });
        
        stage.setOnCloseRequest(event -> {
            backButtonAction();
        });
    }


    @Override
    void backButtonAction() {
        stage.hide();
    }
    public void setTontti(Tonttisuunnitelma tontti){
        this.tontti = tontti;
    }
    @Override
    protected void resetStage() {
        budgetField.clear();
    }
}
