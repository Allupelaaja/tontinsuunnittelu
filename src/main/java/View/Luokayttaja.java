/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;

/**
 *
 * @author Allupelaaja
 */
public class Luokayttaja extends DefaultView{
    static final double LEVEYS = 320;
    static final double KORKEUS = 300;
    private TextField userTextField;
        PasswordField pwBox;
        Text actiontarget;
    /**
     *
     * @param kayttoliittyma
     */
    public Luokayttaja(Kayttoliittyma kayttoliittyma) {
        super(kayttoliittyma, LEVEYS, KORKEUS);  
        logoutBtn.setDisable(true);
        settingsBtn.setDisable(true);
        //Stage
        stage.setTitle("Luo uusi käyttäjä");
        
       //Grid
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10); //Leveys
        grid.setVgap(10); //Korkeus
        grid.setPadding(new Insets(25, 25, 25, 25));
        
        borderPane.setCenter(grid);
        //Tekstikentät
        Text scenetitle = new Text("Luo uusi käyttäjä");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        
        HBox titleHb = new HBox();
        titleHb.getChildren().add(scenetitle);
        titleHb.setAlignment(Pos.CENTER);
        grid.add(titleHb, 0, 0, 2, 1);
        
        
        Label userName = new Label("Käyttäjätunnus:");
        
//        grid.add(userName, 0, 1);
        
        userTextField = new TextField();
//        grid.add(userTextField, 1, 1);
        HBox hbName = new HBox();
        hbName.getChildren().addAll(userName,userTextField);
        hbName.setSpacing(10);
        hbName.setAlignment(Pos.CENTER_RIGHT);
        grid.add(hbName,0,1);
        
        Label pw = new Label("Salasana:");
//        grid.add(pw, 0, 2);

        pwBox = new PasswordField();
//        grid.add(pwBox, 1, 2);
        HBox hbPw = new HBox();
        hbPw.getChildren().addAll(pw,pwBox);
        hbPw.setSpacing(10);
        hbPw.setAlignment(Pos.CENTER_RIGHT);
        grid.add(hbPw,0,2);
        
        
        //
        // NAPIT
        //
        
        actiontarget = new Text();
        actiontarget.wrappingWidthProperty().bind(hbPw.widthProperty());
        grid.add(actiontarget,0,4);
        //Luokayttaja-nappi
        Button createBtn = new Button("Luo käyttäjä");
        HBox createHbBtn = new HBox(10);
        createHbBtn.setAlignment(Pos.CENTER);
        createHbBtn.getChildren().add(createBtn);
        grid.add(createHbBtn, 0, 3);
        //
        //NAPPIEN ACTIONIT
        //
        //Luo käyttäjä action
         createBtn.setOnAction((ActionEvent e) -> {
             String kayttajanimi = userTextField.getText();
             String salasana = pwBox.getText();
             
             if (!kayttajanimi.isEmpty() && !salasana.isEmpty() && 
                kayttajanimi.length() >= 5 && salasana.length() >= 5 &&
                kayttis.luoKayttaja(kayttajanimi, salasana)) {
                backButtonAction();
                } else {
                   actiontarget.setFill(Color.FIREBRICK);
                   actiontarget.setText("Käyttäjänimen ja salasanan oltava vähintään 5 merkkiä");
                }
        });
         
        //Luo käyttäjän enteristä
        scene.addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent t) -> {
            if(t.getCode()==KeyCode.ENTER) {
                createBtn.fire();
            }
        });
        
        stage.setOnCloseRequest(event -> {
            alertAction();
        });
        this.vaakaToolBar.getItems().remove(vaakaToolBar.getItems().size()-2);
    }

    @Override
    void backButtonAction() {
        stage.hide();
        kayttis.showKirjauduStage();
    }
    @Override 
    protected void resetStage() {
        actiontarget.setText("");
        userTextField.clear();
        pwBox.clear(); 
    }
}
