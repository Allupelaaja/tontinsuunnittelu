/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;


import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 *
 * @author aleks
 */
public class Kayttajasivu extends DefaultView {
    static final double LEVEYS = 700;
    static final double KORKEUS = 400;
    private TextField userNewTextField,pwNewTextField,pwOldTextField;
    private Text usernameText,pwText;
    /**
     *
     * @param kayttoliittyma
     */
    public Kayttajasivu(Kayttoliittyma kayttoliittyma) {
        super(kayttoliittyma, LEVEYS, KORKEUS);
        
        //Stage
        stage.setTitle("Käyttäjäsivu");
       
        //Grid
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10); //Leveys
        grid.setVgap(10); //Korkeus
        grid.setPadding(new Insets(25, 25, 25, 25));
        
        borderPane.setCenter(grid);
        //Sisällä oleva grid 1
        GridPane insideGridLeft = new GridPane();
        insideGridLeft.setAlignment(Pos.CENTER);
        insideGridLeft.setHgap(10); //Leveys
        insideGridLeft.setVgap(10); //Korkeus
        insideGridLeft.setPadding(new Insets(25, 25, 25, 25));
        
        
        //Sisällä oleva grid 2
        GridPane insideGridRight = new GridPane();
        insideGridRight.setAlignment(Pos.CENTER);
        insideGridRight.setHgap(10); //Leveys
        insideGridRight.setVgap(10); //Korkeus
        insideGridRight.setPadding(new Insets(25, 25, 25, 25));
        
        //Tekstikentät
        Text scenetitle = new Text("Käyttäjäsivu");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0);
        
        
        //Vasen gridi
        
        //Nimen vaihto vastaus teksti
        
        usernameText = new Text("");
        usernameText.setId("usernameText");
        insideGridLeft.add(usernameText, 0, 2, 2, 1);
        
        Text usernameTitle = new Text("Vaihda käyttäjänimi:");
        insideGridLeft.add(usernameTitle, 0, 0, 2, 1);
        
        Text usernameNewTitle = new Text("Uusi käyttäjänimi:");
        insideGridLeft.add(usernameNewTitle, 0, 1, 2, 1);
        
        
        userNewTextField = new TextField();
        userNewTextField.setId("userNewTextField");
        insideGridLeft.add(userNewTextField, 4, 1);
        
        //Oikea gridi
        
        //Salasanan vaihto vastaus teksti
        
        pwText = new Text("");
        
        insideGridRight.add(pwText, 0, 3, 2, 1);
        
        Text passwordTitle = new Text("Vaihda salasana:");
        insideGridRight.add(passwordTitle, 0, 0, 2, 1);
        
        Text passwordOldTitle = new Text("Vanha salasana:");
        insideGridRight.add(passwordOldTitle, 0, 1, 2, 1);
        
        
        pwOldTextField = new TextField();
        insideGridRight.add(pwOldTextField, 4, 1);
        
        Text passwordNewTitle = new Text("Uusi salasana:");
        insideGridRight.add(passwordNewTitle, 0, 2, 2, 1);
        
        pwNewTextField = new TextField();
        insideGridRight.add(pwNewTextField, 4, 2);
        
        
        
        //Pienemmät gridit ison sisälle
        grid.add(insideGridLeft, 0, 1);
        grid.add(insideGridRight, 2, 1);
        
        //
        // NAPIT
        //
        
        //Vaihda käyttäjänimi -nappi
        Button changeUserBtn = new Button("Vaihda käyttäjänimi");
        changeUserBtn.setId("changeUserNameButton");
        HBox changeUserHbBtn = new HBox(10);
        changeUserHbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        changeUserHbBtn.getChildren().add(changeUserBtn);
        insideGridLeft.add(changeUserHbBtn, 4, 4);
        
        userNewTextField.setOnKeyPressed(e -> {
            if(e.getCode().toString().equals("ENTER")){
                changeUserBtn.fire();
            }
        });
        
        //Vaihda salasana -nappi
        Button changePwBtn = new Button("Vaihda salasana");
        HBox changePwHbBtn = new HBox(10);
        changePwHbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        changePwHbBtn.getChildren().add(changePwBtn);
        insideGridRight.add(changePwHbBtn, 4, 4);
        
        pwNewTextField.setOnKeyPressed(e -> {
            if(e.getCode().toString().equals("ENTER")){
                changePwBtn.fire();
            }
        });
        //
        //NAPPIEN ACTIONIT
        //
        
        //Salasanan vaihto nappi action
        changePwBtn.setOnAction((ActionEvent e) -> {
            String vanha = pwOldTextField.getText();
            String uusi = pwNewTextField.getText();
            
            if (!vanha.isEmpty() && !uusi.isEmpty() && kayttis.vaihdaSalasana(vanha, uusi)) {
                pwText.setFill(Color.GREEN);
                pwText.setText("Salasanan vaihto onnistui");
            } else {
                pwText.setFill(Color.RED);
                pwText.setText("Salasanan vaihto epäonnistui");
            }
        });
        
        //Nimen vaihto nappi action
        changeUserBtn.setOnAction((ActionEvent e) -> {
            String uusi = userNewTextField.getText();
            
            if (!uusi.isEmpty() && !uusi.equals("admin") && kayttis.vaihdaNimi(uusi)) {
                usernameText.setFill(Color.GREEN);
                usernameText.setText("Nimen vaihto onnistui");
                kayttis.setKirjautunutKayttaja(uusi);
                username.setText(uusi);
            }else {
                usernameText.setFill(Color.RED);
                usernameText.setText("Nimen vaihto epäonnistui");
            }
        });
        
        stage.setOnCloseRequest(event -> {
                alertAction();
        });
        
    }



    @Override
    void backButtonAction() {
        stage.hide();
        kayttis.showEtusivuStage();
    }
    @Override
    protected void resetStage() {
        userNewTextField.clear();
        pwNewTextField.clear();
        pwOldTextField.clear();
        usernameText.setText("");
        pwText.setText("");
    }
}
