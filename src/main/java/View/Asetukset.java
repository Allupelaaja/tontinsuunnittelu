/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 *
 * @author aleks
 */
public class Asetukset extends DefaultView {
    static final double LEVEYS = 320;
    static final double KORKEUS = 275;
    CheckBox fullscreenCb;
    ComboBox languageBox;
    
    /**
     *
     * @param kayttoliittyma
     */
    public Asetukset(Kayttoliittyma kayttoliittyma) {
        super(kayttoliittyma, LEVEYS, KORKEUS);
        settingsBtn.setDisable(true);
//        backBtn.setDisable(true);
        logoutBtn.setDisable(true);
        yllapitoBtn.setVisible(false);
        yllapitoBtn.setManaged(false);
        //Stage
        stage.setTitle("Asetukset");
        
        //Gridi
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10); //Leveys
        grid.setVgap(10); //Korkeus
        grid.setPadding(new Insets(25, 25, 25, 25));
        borderPane.setCenter(grid);
                
        //Tekstikentät
        Text scenetitle = new Text("Asetukset");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);
        
        //Checkboxit
        fullscreenCb = new CheckBox("Fullscreen");
        fullscreenCb.setSelected(false);
        grid.add(fullscreenCb, 0, 1, 2, 1);
        
        //Comboboxit
        String kielet[] = {"Suomi", "Deutsch"};
        languageBox = new ComboBox(FXCollections.observableArrayList(kielet));
        languageBox.getSelectionModel().selectFirst();
        Label languageLabel = new Label("Kielivalinta");
        grid.add(languageLabel, 0, 2);
        grid.add(languageBox, 0, 3);
        

        //Fullscreenboxin action
        fullscreenCb.setOnAction((ActionEvent e) -> {
            stage.hide();
            kayttis.vaihdaFullscreen(fullscreenCb.isSelected());
        });
        stage.setOnCloseRequest(event -> {
//                backButtonAction();
        });
    }

    /**
     *
     */
    public void emptyFullscreenbox() {
        fullscreenCb.setSelected(false);
    }


    @Override
    void backButtonAction() {
        stage.hide();
    }
    
}
