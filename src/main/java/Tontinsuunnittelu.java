/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Optional;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author Allupelaaja
 */
public class Tontinsuunnittelu extends Application {
    
    private Stage mainStage;
    private Salaaja salaaja = new Salaaja();
    
    /**
     *
     * @param args
     */
    public static void main(String[] args) {  
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) {
        this.mainStage = primaryStage;
        
        primaryStage.setTitle("Kirjautuminen");
        
        //Grid
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10); //Leveys
        grid.setVgap(10); //Korkeus
        grid.setPadding(new Insets(25, 25, 25, 25));
        
        /*Näyttää gridin viivat, helpottaa elementtien sijoitusta
        grid.setGridLinesVisible(true);
        */
        
        //Tekstikentät
        Text scenetitle = new Text("Tontinsuunnittelu");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);
        
        Label userName = new Label("Käyttäjätunnus:");
        grid.add(userName, 0, 1);
        
        TextField userTextField = new TextField();
        grid.add(userTextField, 1, 1);
        
        Label pw = new Label("Salasana:");
        grid.add(pw, 0, 2);
        
        PasswordField pwBox = new PasswordField();
        grid.add(pwBox, 1, 2);
        
        //Messaget napeille (näiden tilalle siirtymiset!)
        final Text actiontarget = new Text();
        grid.add(actiontarget, 1, 6);
        
        final Text altactiontarget = new Text();
        grid.add(altactiontarget, 1, 10);
        
        final Text createactiontarget = new Text();
        grid.add(createactiontarget, 1, 14);
        
        //
        // NAPIT
        //
        
        //Sisäänkirjautumisnappi
        Button loginBtn = new Button("Kirjaudu sisään");
        HBox loginHbBtn = new HBox(10);
        loginHbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        loginHbBtn.getChildren().add(loginBtn);
        grid.add(loginHbBtn, 1, 4);
        
        //Nappi tuntemattomalle
        
        Button altLoginBtn = new Button("Kirjaudu sisään tuntemattomana");
        HBox altLoginhbBtn = new HBox(10);
        altLoginhbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        altLoginhbBtn.getChildren().add(altLoginBtn);
        grid.add(altLoginhbBtn, 1, 8);
        
        //Nappi uudelle käyttäjälle
        
        Button createLoginBtn = new Button("Luo uusi käyttäjä");
        HBox createLoginHbBtn = new HBox(10);
        createLoginHbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        createLoginHbBtn.getChildren().add(createLoginBtn);
        grid.add(createLoginHbBtn, 1, 12);
        
        //
        //NAPPIEN ACTIONIT
        //
        
        //Kirjautumisnapin action
        loginBtn.setOnAction(new EventHandler<ActionEvent>() {
 
        @Override
        public void handle(ActionEvent e) {
            String kayttajanimi = userTextField.getText();
            String salasana = pwBox.getText();
            if (salaaja.tarkistaKayttaja(kayttajanimi, salasana)) {
                System.out.println(kayttajanimi + " " + salasana);
            };
            
            actiontarget.setFill(Color.FIREBRICK);
            actiontarget.setText("Kirjauduit sisään (tähän ikkunanvaihto)");
            }
        });
        
        //Tuntemattoman kirjautumisnapin action
        altLoginBtn.setOnAction(new EventHandler<ActionEvent>() {
 
        @Override
        public void handle(ActionEvent e) {
            altactiontarget.setFill(Color.FIREBRICK);
            altactiontarget.setText("Kirjauduit sisään tuntemattomana (tähän ikkunanvaihto)");
            }
        });
        
        //Käyttäjänluontibuttonin action
        createLoginBtn.setOnAction(new EventHandler<ActionEvent>() {
 
        @Override
        public void handle(ActionEvent e) {
            createactiontarget.setFill(Color.FIREBRICK);
            createactiontarget.setText("Loit uuden käyttäjän");
            }
        });
        
        //Scene
        Scene scene = new Scene(grid, 300, 275);
        primaryStage.setScene(scene);
        
        primaryStage.show();
        
        //Ohjelman ikkunan ruksin funktio
        primaryStage.setOnCloseRequest(confirmCloseEventHandler);
        
    }
    
    private EventHandler<WindowEvent> confirmCloseEventHandler = event -> {
        //confirmation ikkuna
        Alert closeConfirmation = new Alert(
                Alert.AlertType.CONFIRMATION,
                "Haluatko varmasti sulkea ohjelman?"
        );
        Button exitButton = (Button) closeConfirmation.getDialogPane().lookupButton(
                ButtonType.OK
        );
        exitButton.setText("Poistu");
        closeConfirmation.setHeaderText("Poistumisen vahvistus");
        closeConfirmation.initModality(Modality.APPLICATION_MODAL);
        closeConfirmation.initOwner(mainStage);

        closeConfirmation.setX(mainStage.getX());
        closeConfirmation.setY(mainStage.getY() + mainStage.getHeight());

        Optional<ButtonType> closeResponse = closeConfirmation.showAndWait();
        if (!ButtonType.OK.equals(closeResponse.get())) {
            event.consume();
        }
    };
    
}
