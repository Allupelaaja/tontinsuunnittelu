/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.*;
import View.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Border;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javax.imageio.ImageIO;
//import static sun.security.krb5.Confounder.bytes;
/**
 *
 * @author Allupelaaja
 */
public class Ohjain {
    //private final double INT = 10,ID = 0, BLOB = 10; 
    Tietokanta tk;
    Kayttoliittyma kl;
    Tonttisuunnitelma ts;

    /**
     *
     * @param kl
     */
    public Ohjain (Kayttoliittyma kl) {
        this.tk = new Tietokanta();
        this.kl = kl;
    }
    

    public void setTontti(Tonttisuunnitelma ts){
        this.ts = ts;
    }
    public boolean kirjaudu(String nimi, String salasana){
        return tk.kirjauduSisaan(nimi, salasana);
    }

    /**
     *
     * @param nimi
     * @param salasana
     * @return
     */
    public boolean luoKayttaja(String nimi, String salasana) {
        return tk.luoKayttaja(nimi, salasana);
    }

    /**
     *
     * @param kayttaja
     * @param vanha
     * @param uusi
     * @return
     */
    public boolean vaihdaSalasana(String kayttaja,String vanha, String uusi){
        return tk.vaihdaSalasana(kayttaja,vanha,uusi);
    }

    /**
     *
     * @param vanha
     * @param uusi
     * @return
     */
    public boolean vaihdaNimi(String vanha,String uusi){
        return tk.vaihdaNimi(vanha,uusi);
    }
    
    public void lisaaRuutuun(String x, String y, String alusta, String elementti) {
        ts.lisaaRuutuun(x, y, alusta, elementti);
    }
    
    public void vaihdaRuutu(String x, String y, String alusta, String elementti) {
        ts.vaihdaRuutu(x, y, alusta, elementti);
    }
    
    public String[] haeRuudunKomponentit(int x, int y) {
        return ts.haeRuudunKomponentit(x, y);
    }
    
    public boolean lisaaElementti(String nimi, int tyyppi, String kuvanNimi, byte[] kuva){
        return tk.lisaaElementti(nimi, tyyppi, kuvanNimi,kuva);
    }
    public boolean lisaaMateriaali (String nimi, double hinta, String kuvanNimi, int eTyypi, byte[] kuva){
        return tk.lisaaMateriaali(nimi, hinta, kuvanNimi, eTyypi,kuva);
    }
    public boolean poistaKayttaja(int id){
        return tk.poistaKayttaja(id);
    }
    public boolean poistaMateriaali(int id){
        return tk.poistaMateriaali(id);
    }
    public boolean poistaElementti(int id){
        return tk.poistaElementti(id);
    }
    public TableView getKayttajaTable() {
        TableView table = new TableView();
        
        
        ObservableList<Tontinsuunnittelija> obsList = FXCollections.observableArrayList(tk.getKayttajaTaulu());
        
        TableColumn idCol = new TableColumn("id");
        idCol.setCellValueFactory(
                new PropertyValueFactory<>("id"));
//        idCol.setMinWidth(ID);
        
        TableColumn nameCol = new TableColumn("nimi");
        nameCol.setCellValueFactory(
                new PropertyValueFactory<>("nimi"));
        
        TableColumn rightsCol = new TableColumn("oikeudet");
        rightsCol.setCellValueFactory(
                new PropertyValueFactory<>("oikeudet"));
////        rightsCol.setMinWidth(INT);
        
        TableColumn passCol = new TableColumn("salasana");
        passCol.setCellValueFactory(
                new PropertyValueFactory<>("salasana"));
        
        table.setItems(obsList);
        table.getColumns().addAll(idCol,nameCol,rightsCol,passCol);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        
        return table;
    }
    public TableView getElementtiTable() {
        TableView table = new TableView();
        
        
        ObservableList<Elementti> obsList = FXCollections.observableArrayList(tk.getElementtiTaulu());
        
        TableColumn idCol = new TableColumn("id");
        idCol.setCellValueFactory(
                new PropertyValueFactory<>("id"));
//        idCol.setMinWidth(ID);
        
        TableColumn nameCol = new TableColumn("nimi");
        nameCol.setCellValueFactory(
                new PropertyValueFactory<>("nimi"));
        
        TableColumn typeCol = new TableColumn("tyyppi");
        typeCol.setCellValueFactory(
                new PropertyValueFactory<>("tyyppi"));
////        typeCol.setMinWidth(INT);
        
        TableColumn picNameCol = new TableColumn("kuvan nimi");
        picNameCol.setCellValueFactory(
                new PropertyValueFactory<>("kuvanNimi"));
        
        TableColumn picCol = new TableColumn("kuva");
        picCol.setCellValueFactory(
                new PropertyValueFactory<>("kuva"));
//        picCol.setMinWidth(BLOB);
        
        table.setItems(obsList);
        table.getColumns().addAll(idCol,nameCol,typeCol,picNameCol,picCol);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        
        return table;
    }
    public TableView getMateriaaliTable() {
        TableView table = new TableView();
        
        
        ObservableList<Materiaali> obsList = FXCollections.observableArrayList(tk.getMateriaaliTaulu());
        
        TableColumn idCol = new TableColumn("id");
        idCol.setCellValueFactory(
                new PropertyValueFactory<>("id"));
//        idCol.setMinWidth(ID);
        
        TableColumn nameCol = new TableColumn("nimi");
        nameCol.setCellValueFactory(
                new PropertyValueFactory<>("nimi"));
        
        TableColumn priceCol = new TableColumn("hinta");
        priceCol.setCellValueFactory(
                new PropertyValueFactory<>("hinta"));
        
        TableColumn eTypeCol = new TableColumn("eTyyppi");
        eTypeCol.setCellValueFactory(
                new PropertyValueFactory<>("tyyppi"));
//        //eTypeCol.setMinWidth(INT);
        TableColumn picNameCol = new TableColumn("kuvan nimi");
        picNameCol.setCellValueFactory(
                new PropertyValueFactory<>("kuvanNimi"));
        
        TableColumn picCol = new TableColumn("kuva");
        picCol.setCellValueFactory(
                new PropertyValueFactory<>("kuva"));
//        picCol.setMinWidth(BLOB);
        
        table.setItems(obsList);
        table.getColumns().addAll(idCol,nameCol,priceCol,eTypeCol,picNameCol,picCol);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        
        return table;
    }
    
    public TableView getSuunnitelmaTable() {
        TableView table = new TableView();
        
        
        ObservableList<Tonttisuunnitelma> obsList = FXCollections.observableArrayList(tk.getSuunnitelmaTaulu());
        
        TableColumn idCol = new TableColumn("id");
        idCol.setCellValueFactory(
                new PropertyValueFactory<>("id"));
//        idCol.setMinWidth(ID);
        
        TableColumn nameCol = new TableColumn("nimi");
        nameCol.setCellValueFactory(
                new PropertyValueFactory<>("nimi"));
        
        TableColumn ownerCol = new TableColumn("omistaja");
        
        TableColumn budgetCol = new TableColumn("budjetti");
        budgetCol.setCellValueFactory(
                new PropertyValueFactory<>("budjetti"));
        
        TableColumn floorCol = new TableColumn("alusta");
        floorCol.setCellValueFactory(
                new PropertyValueFactory<>("alusta"));
        
        TableColumn heightCol = new TableColumn("korkeus");
        heightCol.setCellValueFactory(
                new PropertyValueFactory<>("korkeus"));
        
        TableColumn widthCol = new TableColumn("leveys");
        widthCol.setCellValueFactory(
                new PropertyValueFactory<>("leveys"));
        
        TableColumn matsCol = new TableColumn("sisalto");
        matsCol.setCellValueFactory(
                new PropertyValueFactory<>("blobArray"));
//        matsCol.setMinWidth(BLOB);
        
        TableColumn picCol = new TableColumn("kuva");
        picCol.setCellValueFactory(
                new PropertyValueFactory<>("kuva"));
//        picCol.setMinWidth(BLOB);
        
        TableColumn ownerIdCol = new TableColumn("id");
//        ownerIdCol.setMinWidth(ID);
        
        TableColumn ownerNameCol = new TableColumn("nimi");
        
        ownerIdCol.setCellValueFactory(
                new PropertyValueFactory<>("omistajaId"));
        ownerNameCol.setCellValueFactory(
                new PropertyValueFactory<>("omistajaNimi"));
        
        ownerCol.getColumns().addAll(ownerIdCol,ownerNameCol);
        
        table.setItems(obsList);
        table.getColumns().addAll(idCol,nameCol,ownerCol, budgetCol,   matsCol,picCol);// floorCol, heightCol,widthCol, 
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        
        return table;
    }
    
    public ArrayList<FlowPane> getMateriaaliPanet() {
        ArrayList<Materiaali> mList = (ArrayList<Materiaali>) tk.getMateriaaliTaulu();
        ArrayList<Elementti> eList = (ArrayList<Elementti>) tk.getElementtiTaulu();
        ArrayList<FlowPane> panet = new ArrayList<>();
        for (int i =1; i <= eList.size(); i++){
            FlowPane pane = new FlowPane();
            for(Materiaali m: mList){
                if(m.getTyyppi() == i){
                    Button b = new Button(m.getNimi(),new ImageView(new Image("file:"+m.getKuvanNimi())));
                    b.setTooltip(new Tooltip(m.getNimi()));
                    pane.getChildren().add(b);
                }
            }
            panet.add(pane);
        }
        return panet;
    }
    public ArrayList<Button> getElementtiButtons() {
        ArrayList<Elementti> eList = (ArrayList<Elementti>) tk.getElementtiTaulu();
        ArrayList<Button> buttons = new ArrayList<>();
        for(Elementti e:eList){
            Button b = new Button(e.getNimi(),new ImageView(new Image("file:"+e.getKuvanNimi())));
            b.setTooltip(new Tooltip(e.getNimi()));
            buttons.add(b);
        }
        return buttons;
    }
    private static List<Materiaali> mList = null;
    public String getMaterialPicName(String materialName){
        if(mList == null){
            mList = tk.getMateriaaliTaulu();
        }
        for(Materiaali m:mList){
            if(m.getNimi().equals(materialName)){
                return m.getKuvanNimi();
            }
        }
        return null;
    }
    public List<String> getAlustat() {
        List<Materiaali> mList = tk.getMateriaaliTaulu();
        ArrayList<String> alustat = new ArrayList<>(); 
        for(Materiaali m:mList){
            if(m.getTyyppi() == 1){
                alustat.add(m.getNimi());
            }
        }
        return alustat;
    }
    public boolean tallennaSuunnitelma(Tonttisuunnitelma suunnitelma){
        return tk.tallennaSuunnitelma(suunnitelma);
    }
    public Tonttisuunnitelma getSuunnitelma (int id){
        List<Tonttisuunnitelma> listSuunnitelmat = tk.getSuunnitelmaTaulu();
        for(Tonttisuunnitelma suunnitelma : listSuunnitelmat){
            if(suunnitelma.getId() == id){
                return suunnitelma;
            }
        }
        return null;
    }
    public Tontinsuunnittelija getKayttaja (String nimi){
        List<Tontinsuunnittelija> kList = tk.getKayttajaTaulu();
        for(Tontinsuunnittelija x : kList){
            if(x.getNimi().equals(nimi)){
                return x;
            }
        }
        System.out.println("Annettua käyttäjää ei löytynyt");
        return null;
    }
    public boolean setOmistaja(String omistaja){
        System.out.println("Omistaja: "+omistaja);
        List<Tontinsuunnittelija> kList = tk.getKayttajaTaulu();
        for(Tontinsuunnittelija x : kList){
            if(x.getNimi().equals(omistaja)){
                ts.setOmistaja(x);
                return true;
            }
        }
        System.out.println("Omistajan asettamimnen epäonnistui");
        return false;
    }
    public boolean päivitäOikeudet (int id, int oikeus) {
        return tk.päivitäOikeudet(id, oikeus);
    }
    public boolean poistaSuunnitelma(int id) {
        return tk.poistaSuunnitelma(id);
    }
//    public boolean poistaSuunnitelma(Object o) {
//        Tonttisuunnitelma t = (Tonttisuunnitelma) o;
//        return tk.poistaSuunnitelma(t.getId());
//    }
    public void tallennaKuvat (){
        tk.tallennaKuvat();
    }
    public Tonttisuunnitelma getSuunnitelma(String omistaja, String suunnitelmaNimi) {
        List<Tonttisuunnitelma> sList = tk.getSuunnitelmaTaulu();
        for(Tonttisuunnitelma s : sList){
            if(s.getNimi().equals(suunnitelmaNimi) && s.getOmistajaNimi().equals(omistaja)){
                return s;
            }
        }
        System.out.println("Suunnitelmaa "+suunnitelmaNimi+" ei löytynyt");
        return null;
    }
    public List<Tonttisuunnitelma> getSuunnitelmaTaulu(){
        return tk.getSuunnitelmaTaulu();
    }
    public int getKayttajaOikeus(String nimi){
        return tk.getKayttajaOikeus(nimi);
    }
    public ArrayList<GridPane> getSuunnitelmaGrids (String omistaja) {
       ArrayList<GridPane> grids = new ArrayList<>();
       List<Tonttisuunnitelma> sList = tk.getSuunnitelmaTaulu();
       for(Tonttisuunnitelma ts : sList){
           if(ts.getOmistajaNimi().equals(omistaja)){
               try {
                   BufferedImage img = ImageIO.read(new ByteArrayInputStream(ts.getKuva()));
                   GridPane grid = new GridPane();
                   grid.setMaxWidth(190);
//                   grid.setVgap(2);
                    grid.setHgap(2);
                    
                   ImageView imgView = new ImageView(SwingFXUtils.toFXImage(img, null));
                   imgView.setFitHeight(100);
                   imgView.setFitWidth(100);

                   Text gridTitle = new Text(ts.getNimi());
                   HBox titleHB = new HBox();
                   titleHB.getChildren().add(gridTitle);
                   gridTitle.setFont(Font.font("Tahoma", FontWeight.BOLD, 20));
//                    gridTitle.setAlignment(Pos.CENTER);
                   titleHB.setAlignment(Pos.CENTER);
//                    gridTitle.setWrapText(true);


//                    Text gridOwner = new Text(ts.getOmistajaNimi());
//                   HBox ownerHB = new HBox();
//                   ownerHB.getChildren().add(gridOwner);
//                   gridOwner.setFont(Font.font("Tahoma", FontWeight.THIN, 15));
////                    gridTitle.setAlignment(Pos.CENTER);
//                   ownerHB.setAlignment(Pos.CENTER);
////                    gridTitle.setWrapText(true);

                   Button openBtn = new Button("Avaa");
                   openBtn.setMinWidth(90);
                   openBtn.setMinHeight(31);
                   openBtn.setTooltip(new Tooltip(openBtn.getText()));

                   Button delBtn = new Button("Poista");
                   delBtn.setMinWidth(90);
                   delBtn.setMinHeight(31);
                   delBtn.setTooltip(new Tooltip(delBtn.getText()));

                   Button shareBtn = new Button("Jaa");
                   shareBtn.setMinWidth(90);
                   shareBtn.setMinHeight(31);
                   shareBtn.setTooltip(new Tooltip(shareBtn.getText()));

                   shareBtn.setDisable(ts.isJaettu());

                   grid.add(titleHB,   0,  0,  2,  1);
                   grid.add(imgView,   0,  1,  1,  3);
                   grid.add(openBtn,   1,  1);
                   grid.add(delBtn,    1,  2);
                   grid.add(shareBtn,  1,  3);
//                   grid.add(ownerHB,   1,  4);
                   grids.add(grid);
               } catch (IOException ex) {
                   ex.printStackTrace();
               }
           }
       }
       return grids;
   }
    public ArrayList<GridPane> getJaetutSuunnitelmaGrids () {
       ArrayList<GridPane> grids = new ArrayList<>();
       List<Tonttisuunnitelma> sList = tk.getSuunnitelmaTaulu();
       for(Tonttisuunnitelma ts : sList){
           if(ts.isJaettu()){
               try {
                   BufferedImage img = ImageIO.read(new ByteArrayInputStream(ts.getKuva()));
                   GridPane grid = new GridPane();
                   grid.setMaxWidth(190);
//                   grid.setVgap(2);
                    grid.setHgap(2);

                   ImageView imgView = new ImageView(SwingFXUtils.toFXImage(img, null));
                   imgView.setFitHeight(100);
                   imgView.setFitWidth(100);

                   Text gridTitle = new Text(ts.getNimi());
                   HBox titleHB = new HBox();
                   titleHB.getChildren().add(gridTitle);
                   gridTitle.setFont(Font.font("Tahoma", FontWeight.BOLD, 20));
//                    gridTitle.setAlignment(Pos.CENTER);
                   titleHB.setAlignment(Pos.CENTER);
//                    gridTitle.setWrapText(true);


                   Text gridOwner = new Text(ts.getOmistajaNimi());
                   HBox ownerHB = new HBox();
                   ownerHB.getChildren().add(gridOwner);
                   gridOwner.setFont(Font.font("Tahoma", FontWeight.THIN, 15));
//                    gridTitle.setAlignment(Pos.CENTER);
                   ownerHB.setAlignment(Pos.CENTER);
//                    gridTitle.setWrapText(true);

                   Button openBtn = new Button("Avaa");
                   openBtn.setMinWidth(90);
////                   openBtn.setMinHeight(29);
                   openBtn.setTooltip(new Tooltip(openBtn.getText()));
                   
                   grid.add(titleHB,   0,  0,  2,  1);
                   grid.add(imgView,   0,  1,  1,  4);
                   grid.add(openBtn,   1,  1);
                   grid.add(ownerHB,   1,  4);
                   
                   grids.add(grid);
               } catch (IOException ex) {
                   ex.printStackTrace();
               }
           }
       }
       return grids;
   }
     public boolean poistaSuunnitelma(String suunnitelmaNimi, String omistaja) {
         return tk.poistaSuunnitelma(tk.getSuunnitelmaId(suunnitelmaNimi, omistaja));
     }
     
     public void jaaSuunnitelma(String suunnitelmaNimi, String omistaja){
         tk.jaaSuunnitelma(suunnitelmaNimi, omistaja);
     }
     public void jaaSuunnitelma(Tonttisuunnitelma tontti){
         tk.jaaSuunnitelma(tontti);
//         System.out.println("Ohjain: "+tk.jaaSuunnitelma(tontti));
     }
     
}
