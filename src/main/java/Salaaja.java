    
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;




/**
 *
 * @author timok
 */
public class Salaaja {
    
    private int hash;
    private int pituus;
    
    /**
     *
     */
    public Salaaja() {
        hash = 0;
        pituus = 0;
    }
       
    /**
     *
     * @param salasana
     * @return
     */
    public int salaa(String salasana) {
        pituus = salasana.length();
        char[] chartaulu = salasana.toCharArray();
        int[] inttaulu = new int[pituus];
        for (int i = 0; i < pituus; i++) {
            inttaulu[i] = (int) chartaulu[i];
            hash = hash + inttaulu[i] * inttaulu[i] * (i+1);
        }
        return hash;
    }
    
    /**
     *
     * @param kayttajanimi
     * @param salasana
     * @return
     */
    public boolean tarkistaKayttaja(String kayttajanimi, String salasana) {
        boolean oikea = false;
        
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://10.114.34.72/tontinsuunnittelu", "username", "password")) {
            // create a Statement
            try (Statement stmt = conn.createStatement()) {
                //execute query
                try (ResultSet rs = stmt.executeQuery("SELECT * FROM kayttajat")) {
                    //position result to first
                    while ( rs.next() ) {
                        String dbNimi = rs.getString("nimi");
                        String dbSalasana = rs.getString("salasana");
                        if (kayttajanimi.equals(dbNimi)) {
                            if (salasana.equals(dbSalasana)) {
                                oikea = true;
                                return oikea;
                            }
                        }
                    }
                    
                }
            }
        } catch (Exception e) {
            
        }
        
        return oikea;
    }   
}
