/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;


import javax.persistence.*;



/**
 *
 * @author Allupelaaja
 */

@Entity
@Table(name="materiaalit")
public class Materiaali {
    private int id;
    private String nimi;
    private double hinta;
    private String kuvanNimi;
    private int tyyppi;
    private byte[] kuva;
    
    
    public Materiaali() {
        
    }

    public Materiaali(String nimi, double hinta, String kuvanNimi, int eTyyppi, byte[] kuva) {
        this.nimi = nimi;
        this.hinta = hinta;
        this.kuvanNimi = kuvanNimi;
        this.tyyppi = eTyyppi;
        this.kuva = kuva;
    }
    
    
    @Column(name="kuva")
    public byte[] getKuva() {
        return kuva;
    }

    public void setKuva(byte[] kuva) {
        this.kuva = kuva;
    }
    
    @Column(name="kuvanNimi")
    public String getKuvanNimi() {
        return kuvanNimi;
    }

    public void setKuvanNimi(String kuvanNimi) {
        this.kuvanNimi = kuvanNimi;
    }
    /**
     *
     * @return
     */
    @Column(name="nimi")
    public String getNimi() {
        return nimi;
    }
    
    /**
     *
     * @param nimi
     */
    public void setNimi(String nimi) {
        this.nimi = nimi;
    }
    
    /**
     *
     * @return
     */
    @Column(name="hinta")
    public double getHinta() {
        return hinta;
    }

    /**
     *
     * @param hinta
     */
    public void setHinta(double hinta) {
        this.hinta = hinta;
    }
    
    /**
     *
     * @return
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }
    
    @Column(name="tyyppi",nullable = false)
    public int getTyyppi() {
        return tyyppi;
    }

    public void setTyyppi(int tyyppi) {
        this.tyyppi = tyyppi;
    }
    
    
    
}

