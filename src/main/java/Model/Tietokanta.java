/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import javafx.stage.FileChooser;
//import javax.security.auth.login.Configuration;
import org.hibernate.*;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;


/**
 *
 * @author Allupelaaja, timok
 */
public class Tietokanta {
    /*private Connection conn;
    private Statement stmt;*/
    
    SessionFactory istuntotehdas = null;
    //Käyttäjäoikeudet
   private static boolean filesIsCopied = false;
   // private static final int VIERAILIJA = 0,PERUSKAYTTAJA = 1,TARKISTAJA = 2, YLLAPITAJA = 3;
    FileChooser fileChooser = new FileChooser();
    File valittuFile;
    /**
     * Konstruktori avaa tietokantayhteydelle istuntotehtaan
     */
    public Tietokanta() {
        StandardServiceRegistry registry = null;
        try{
            istuntotehdas = new Configuration().configure("testiConf.cfg.xml").buildSessionFactory();
        }catch (Exception e ){
            System.out.println("Jenkins tietokanta ongelma");
            e.printStackTrace();
        }
        try{
//            registry = new StandardServiceRegistryBuilder().configure("testiConf.cfg.xml").build();
//            istuntotehdas = new MetadataSources(registry).buildMetadata().buildSessionFactory();
            
            if(istuntotehdas == null){
                System.out.println("oli null");
                registry = new StandardServiceRegistryBuilder().configure().build();
                istuntotehdas = new MetadataSources(registry).buildMetadata().buildSessionFactory();
            }
            
            
            System.out.println("Tietokanta yhteys pelittää!");
        } catch(Exception e){
            System.out.println("Tietokanta yhteys ei nyt oikeen pelitä");
            e.printStackTrace();
            StandardServiceRegistryBuilder.destroy(registry);
        }
        
    }

    /**
     * Käyttäjän poistaminen tietokannasta
     * 
     * @param nimi käyttäjänimi
     * @return true jos käyttäjän poistaminen onnistui
     */
    public boolean poistaKayttaja (String nimi){
        Session istunto = istuntotehdas.openSession();
        Transaction transaktio = null;
        if(nimi.equals("admin")){
            System.out.println("Et voi poistaa admin käyttäjää");
            return false;
        }
        try {
            int id = getKayttajaId(nimi);
            //Tarkistetaan onko käyttäjänimi jo käytössä
            if(id != 0){
            transaktio = istunto.beginTransaction();
            Tontinsuunnittelija kayttaja = (Tontinsuunnittelija)istunto.get(Tontinsuunnittelija.class, id);
            istunto.delete(kayttaja);
            transaktio.commit();
                return true;
            } else {
                return false;
            }
            } catch(Exception e) {
                System.out.println("Käyttäjän poistossa ongelmia nimellä");
                if (transaktio!=null){
                    transaktio.rollback();
                }
                throw e;
                } finally {
                istunto.close();
            }
    }
    
    public boolean poistaKayttaja (int id){
        Session istunto = istuntotehdas.openSession();
        Transaction transaktio = null;
        
        try {
            //Tarkistetaan onko käyttäjänimi jo käytössä
            if(id != 0){
            transaktio = istunto.beginTransaction();
            Tontinsuunnittelija kayttaja = (Tontinsuunnittelija)istunto.get(Tontinsuunnittelija.class, id);
            if(kayttaja.getNimi().equals("admin")){
                System.out.println("Et voi poistaa admin käyttäjää");
                return false;
            }
            istunto.delete(kayttaja);
            transaktio.commit();
                return true;
            } else {
                return false;
            }
            } catch(Exception e) {
                System.out.println("Käyttäjän poistossa ongelmia id:llä");
                if (transaktio!=null){
                    transaktio.rollback();
                }
                throw e;
                } finally {
                istunto.close();
            }
    }
    /**
     * Käyttäjän lisääminen tietokantaan
     * 
     * @param nimi käyttäjänimi
     * @param salasana käyttäjän salasana
     * @return true jos käyttäjän lisääminen tietokantaan onnistui
     */
    public boolean luoKayttaja(String nimi, String salasana) {
        if(nimi.equals("")||salasana.equals("")){
            return false;
        }
        Session istunto = istuntotehdas.openSession();
        Transaction transaktio = null;
        int salattu_salasana = salaa(salasana);
        try {
            //Tarkistetaan onko käyttäjänimi jo käytössä
            if(getKayttajaId(nimi) == 0){
            transaktio = istunto.beginTransaction();
            Tontinsuunnittelija kayttaja = new Tontinsuunnittelija(nimi, salattu_salasana);
            istunto.saveOrUpdate(kayttaja);
            transaktio.commit();
                return true;
            } else {
                return false;
            }
            } catch(Exception e) {
                System.out.println("Käyttäjän luonnissa ongelmia");
                if (transaktio!=null){
                    transaktio.rollback();
                }
                throw e;
                } finally {
                istunto.close();
            }
    }
     
    /**
     * Päivittää käyttäjän oikeusluokan
     * 
     * @param id käyttäjäid
     * @param oikeus käyttäjän oikeusluokka
     * @return true jos päivitys onnistui
     */
    public boolean päivitäOikeudet (int id, int oikeus) {
         try (Session istunto = istuntotehdas.openSession()){
            istunto.beginTransaction();
            Tontinsuunnittelija kayttaja = (Tontinsuunnittelija)istunto.get(Tontinsuunnittelija.class, id);
            kayttaja.setOikeudet(oikeus);
            istunto.getTransaction().commit();
            return true;
         } catch (Exception e) {
             System.out.println("Oikeuden päivittämisessä ongelmia");
             e.printStackTrace();
             return false;
         }
     }
    
    /**
     * Päivittää käyttäjän oikeusluokan
     * 
     * @param nimi käyttäjänimi
     * @param oikeus käyttäjän oikeusluokka
     */
    public boolean päivitäOikeudet (String nimi, int oikeus) {

         int id = getKayttajaId(nimi);
         if(id!= 0){
             return päivitäOikeudet(id, oikeus);
         }else{
             return false;
         }
        
     }

    /**
     * Vaihtaa käyttäjän salasanan
     * 
     * @param nimi käyttäjänimi
     * @param vanha_salasana käyttäjän vanha salasana
     * @param uusi_salasana käyttäjän uusi salasana
     * @return true jos salasanan vaihtaminen onnistui
     */
    public boolean vaihdaSalasana (String nimi, String vanha_salasana, String uusi_salasana ) {
         // Tiedon haku Session.get()-metodilla + päivitys
         Session istunto = istuntotehdas.openSession();
         int id = getKayttajaId(nimi);
         
         //Katsotaan onko käyttäjää olemassa annetulla nimellä.
         if(id == 0){
             System.out.println("Annettua käyttäjää ei ollut tietokannassa");
             return false;
         }
         
         int salattu_uusi = salaa(uusi_salasana);
         int salattu_vanha = salaa(vanha_salasana);
         try {
            istunto.beginTransaction();
            Tontinsuunnittelija kayttaja = (Tontinsuunnittelija)istunto.get(Tontinsuunnittelija.class, id);
        if (kayttaja!= null && ( salattu_vanha == kayttaja.getSalasana() )){
            kayttaja.setSalasana(salattu_uusi);
            istunto.getTransaction().commit();
            System.out.println("Salasana päivitetty!");
            return true;
        } else {
            System.out.println("Salasana ei päivitetty, annettu vanha salasana ei täsmännyt");
            return false;
        }
        
         } catch (Exception e) {
             System.out.println("Salasanan vaihdossa ongelmia");
             return false;
         }finally {
            istunto.close();
        }
        
     }

    /**
     * Vaihtaa käyttäjän käyttäjänimen
     * 
     * @param vanha vanha käyttäjänimen
     * @param uusi uusi käyttäjänimi
     * @return true jos nimenvaihto onnistui
     */
    public boolean vaihdaNimi (String vanha, String uusi ) {
         // Tiedon haku Session.get()-metodilla + päivitys
         Session istunto = istuntotehdas.openSession();
         int id = getKayttajaId(vanha);
         
         //Katsotaan onko käyttäjää olemassa annetulla nimellä.
         if(id == 0){
             System.out.println("Annettua käyttäjää ei ollut tietokannassa");
             return false;
         }
         
         try {
            istunto.beginTransaction();
            Tontinsuunnittelija kayttaja = (Tontinsuunnittelija)istunto.get(Tontinsuunnittelija.class, id);
            if (kayttaja!= null && kayttaja.getNimi().equals(vanha)){
                if(getKayttajaId(uusi) != 0){
                    System.out.println("Käyttäjänimi "+uusi+" on jo käytössä");
                    return false;
                }
                kayttaja.setNimi(uusi);
                istunto.getTransaction().commit();
                System.out.println("Käyttäjänimi päivitetty!");
                return true;
            } else {
                System.out.println("Käyttäjänimi ei päivitetty, annettu vanha nimi ei täsmännyt");
                return false;
            }
        
         } catch (Exception e) {
             System.out.println("Nimen vaihdossa ongelmia");
             return false;
         }finally {
            istunto.close();
        }
        
     }
     
  
    /**
     * Hakee tietokannasta käyttäjän id:n
     * 
     * @param nimi käyttäjänimi
     * @return käyttäjän id, jos käyttäjää ei ole niin palauttaa 0.
     */
    public int getKayttajaId(String nimi){
      // Tiedon haku (kaikki valuutat) (Read)
      Session istunto = istuntotehdas.openSession();
        try {
        istunto.beginTransaction();
        List<Tontinsuunnittelija> result = istunto.createQuery( "from Tontinsuunnittelija" ).getResultList();
        for ( Tontinsuunnittelija kayttaja : result ) {
            //System.out.println(kayttaja.getId() +"  "+kayttaja.getNimi() + " "+ kayttaja.getSalasana());
            if (kayttaja.getNimi().equals(nimi)){
                return kayttaja.getId();
            }
        }
        } catch (Exception e){
           System.out.println("Kayttaja id:n hakemisessa onkelmia");
            e.printStackTrace();
        } finally {
            istunto.close();
           
        }
        return 0;
    }
    /**
     * Tarkistaa käyttäkän oikeudet
     * @param nimi käyttäjänimi
     * @return käyttäjän oikeudet -1 = Ongelmia käyttäjänimen haussa, 1 = Kirjautunut, 2 = Tarkistaja, 3 = Ylläpitäjä
     */
    public int getKayttajaOikeus(String nimi){
      
      int id = getKayttajaId(nimi);
      if(id == 0){
          System.out.println("Käyttäjänimi "+ nimi+" Ei löytynyt tietokanssasta");
          return -1;
      }
        try(Session istunto = istuntotehdas.openSession()) {
        Tontinsuunnittelija kayttaja = (Tontinsuunnittelija)istunto.get(Tontinsuunnittelija.class, id);
        return kayttaja.getOikeudet();
        } catch (Exception e){
           System.out.println("Kayttajan oikeuksien hakemisessa ongelmia");
            e.printStackTrace();
            return -1;
        } 
    }
    /**
     * Suorittaa ohjelmaan sisäänkirjautumisen
     * 
     * @param nimi käyttäjänimi
     * @param salasana käyttäjän salasana
     * @return true jos kirjautuminen onnistui
     */
    public boolean kirjauduSisaan (String nimi, String salasana) {
        // Tiedon haku avaimen perusteella (Read)
        Session istunto = istuntotehdas.openSession();
        int salattu_salasana = salaa(salasana);
        int id = getKayttajaId(nimi);
        if (id ==0){
            System.out.println("Käyttäjänimeä ei löytynyt");
            return false;
        }
        try {
        istunto.beginTransaction();
        
        Tontinsuunnittelija kayttaja = new Tontinsuunnittelija();
        istunto.load(kayttaja, id);
        istunto.getTransaction().commit();
        if(kayttaja.getSalasana() == salattu_salasana){
            System.out.println("Kirjautuminen onnistui!");
            return true;
        }
        
        } catch (Exception e){
            System.out.println("Kirjautumisessa ongelmia");
            e.printStackTrace();
           } finally {
            istunto.close();
            
        }
        return false;
    }
    /**
     * Palauttaa elementti taulun rivit
     * 
     * @return elementti taulun rivit
     */
    
    public List<Elementti> getElementtiTaulu(){
        List<Elementti> result;
        try (Session istunto = istuntotehdas.openSession()) {
        istunto.beginTransaction();
            result = istunto.createQuery( "from Elementti" ).getResultList();
            return result;
        } catch (Exception e) {
            System.out.println("Elementti taulun haussa ongelmia");
            e.printStackTrace();
            return null;
        }
        
    }   
    /**
     * Palauttaa käyttäjä taulun rivit
     * 
     * @return käyttäjä taulun rivit
     */
    
    public List<Tontinsuunnittelija> getKayttajaTaulu(){
        List<Tontinsuunnittelija> result;
        try (Session istunto = istuntotehdas.openSession()) {
        istunto.beginTransaction();
            result = istunto.createQuery( "from Tontinsuunnittelija" ).getResultList();
            return result;
        } catch (Exception e) {
            System.out.println("Käyttäjä taulun haussa ongelmia");
            e.printStackTrace();
            return null;
        }
        
    }
    /**
     * Palauttaa materiaali taulun rivit
     * 
     * @return materiaali taulun rivit
     */
    
    public List<Materiaali> getMateriaaliTaulu(){
        List<Materiaali> result;
        try (Session istunto = istuntotehdas.openSession()) {
        istunto.beginTransaction();
            result = istunto.createQuery( "from Materiaali" ).getResultList();
            return result;
        } catch (Exception e) {
            System.out.println("Materiaali taulun haussa ongelmia");
            e.printStackTrace();
            return null;
        }
        
    }
     /**
     * Palauttaa tonttisuunitelmat taulun rivit
     * 
     * @return tonttisuunitelmat taulun rivit
     */
    
    public List<Tonttisuunnitelma> getSuunnitelmaTaulu(){
        List<Tonttisuunnitelma> result;
        try (Session istunto = istuntotehdas.openSession()) {
        istunto.beginTransaction();
            result = istunto.createQuery( "from Tonttisuunnitelma" ).getResultList();
            return result;
        } catch (Exception e) {
            System.out.println("Tonttisuunnitelma taulun haussa ongelmia");
            e.printStackTrace();
            return null;
        }
        
    }
    
    public boolean lisaaMateriaali(String nimi, double hinta, String kuvanNimi, int eTyypi, byte[] kuva) {
        
        //Tarkistetaan on annetun niminen materiaali jo olemassa
        if (!getMateriaaliTaulu().stream().noneMatch((x) -> (x.getNimi().equals(nimi)))) {
            System.out.println(nimi+ " materiaali on jo olemassa");
            return false;
        }
        
        Transaction transaktio = null;
       
        try (Session istunto = istuntotehdas.openSession()){
            transaktio = istunto.beginTransaction();
            Materiaali materiaali = new Materiaali(nimi, hinta, kuvanNimi, eTyypi,kuva);
            istunto.saveOrUpdate(materiaali);
            transaktio.commit();
                return true;
            } catch(Exception e) {
                if (transaktio!=null){
                    transaktio.rollback();
                }
                System.out.println("Materiaalin luonnissa ongelmia");
                e.printStackTrace();
                return false;
                }
    }
    public boolean poistaMateriaali(int id) {
        
        //Tarkistetaan on annetun niminen materiaali olemassa
        if (getMateriaaliTaulu().stream().noneMatch((x) -> (x.getId() == id))) {
            System.out.println("id: "+ id+ " materiaalia ei ole olemassa");
            return false;
        }
        
        Transaction transaktio = null;
       
        try (Session istunto = istuntotehdas.openSession()){
            transaktio = istunto.beginTransaction();
            Materiaali materiaali = (Materiaali)istunto.get(Materiaali.class, id);
            istunto.delete(materiaali);
            transaktio.commit();
                return true;
            } catch(Exception e) {
                if (transaktio!=null){
                    transaktio.rollback();
                }
                System.out.println("Materiaalin poistossa ongelmia");
                e.printStackTrace();
                return false;
                }
    }
    public boolean poistaElementti(int id) {
        
        //Tarkistetaan on annettu elementti id olemassa
        if (getElementtiTaulu().stream().noneMatch((x) -> (x.getId() == id))) {
            System.out.println("id: "+ id+ " elementtiä ei ole olemassa");
            return false;
        }
        
        Transaction transaktio = null;
       
        try (Session istunto = istuntotehdas.openSession()){
            transaktio = istunto.beginTransaction();
            Elementti elementti  = (Elementti)istunto.get(Elementti.class, id);
            istunto.delete(elementti);
            transaktio.commit();
                return true;
            } catch(Exception e) {
                if (transaktio!=null){
                    transaktio.rollback();
                }
                System.out.println("Elementin poistossa ongelmia");
                e.printStackTrace();
                return false;
                }
    }
    public boolean poistaSuunnitelma(int id) {
        
        //Tarkistetaan on annetun niminen suunnitelma olemassa
        if (getSuunnitelmaTaulu().stream().noneMatch((x) -> (x.getId() == id))) {
            System.out.println("id: "+ id+ " suunnitelmaa ei ole olemassa");
            return false;
        }
        Transaction transaktio = null;
       
        try (Session istunto = istuntotehdas.openSession()){
            transaktio = istunto.beginTransaction();
            Tonttisuunnitelma tontti  = (Tonttisuunnitelma)istunto.get(Tonttisuunnitelma.class, id);
            istunto.delete(tontti);
            transaktio.commit();
                return true;
            } catch(Exception e) {
                if (transaktio!=null){
                    transaktio.rollback();
                }
                System.out.println("Suunnitelman poistossa ongelmia");
                e.printStackTrace();
                return false;
                }
    }
    
    public int getSuunnitelmaId(String nimi, String omistaja){
        List<Tonttisuunnitelma> sList = getSuunnitelmaTaulu();
        for (Tonttisuunnitelma ts : sList ) {
            if (ts.getNimi().equals(nimi) && ts.getOmistajaNimi().equals(omistaja)){
                return ts.getId();
            }
        }
        return 0;
    }
    public boolean lisaaElementti(String nimi, int tyyppi, String kuvanNimi, byte[] kuva) {
        
        //Tarkistetaan on annetun niminen elementti jo olemassa
        if (!getElementtiTaulu().stream().noneMatch((x) -> (x.getNimi().equals(nimi)))) {
            System.out.println(nimi+ " elementti on jo olemassa");
            return false;
        }
        
        Transaction transaktio = null;
       
        try (Session istunto = istuntotehdas.openSession()){
            transaktio = istunto.beginTransaction();
            Elementti elementti = new Elementti(nimi,tyyppi,kuvanNimi,kuva);
            istunto.saveOrUpdate(elementti);
            transaktio.commit();
                return true;
            } catch(Exception e) {
                if (transaktio!=null){
                    transaktio.rollback();
                }
                System.out.println("Elementit luonnissa ongelmia");
                e.printStackTrace();
                return false;
                }
    }
    /**
     * Salaa annetun salasana
     * 
     * @param salasana käyttäjän salasana
     * @return salattu salasana
     */
    public int salaa(String salasana) {
        int hash = 0;
        int pituus = salasana.length();
        char[] chartaulu = salasana.toCharArray();
        int[] inttaulu = new int[pituus];
        for (int i = 0; i < pituus; i++) {
            inttaulu[i] = (int) chartaulu[i];
            hash = hash + inttaulu[i] * inttaulu[i] * (i+1);
        }
        while (hash < 10000000 || hash > 99999999) {
            if (hash == 0) {
                hash = 10000000;
            }
            if (hash < 0) {
                hash = -hash;
            }
            if (hash < 10000000) {
                hash = hash * 9;
            }
            if (hash > 99999999) {
                hash = hash / 9;
            }
        }
        return hash;
    }
    
     public void tallennaKuvat (){
        if(filesIsCopied){
            return;
        }
        List<Materiaali> mTaulu = getMateriaaliTaulu();
        List<Elementti> eTaulu = getElementtiTaulu();
        eTaulu.forEach((e) -> {
            try {
                File file = new File(e.getKuvanNimi());
                if(!file.exists()){
                    OutputStream os = new FileOutputStream(file);
                    os.write(e.getKuva());
                    System.out.println("Tiedosto: "+file.getName()+" tallennettu paikkaan: "+file.getPath());
                    os.close();
                }
                System.out.println("Tiedosto: "+file.getAbsolutePath()+" on jo olemassa");
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        mTaulu.forEach((m) -> {
            try {
                File file = new File(m.getKuvanNimi());
                if(!file.exists()){
                OutputStream os = new FileOutputStream(file); 
                os.write(m.getKuva());
                os.close();
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        });
        filesIsCopied = true;
         System.out.println("Tiedostot kopioitu tietokannasta.");
    }
    public boolean tallennaSuunnitelma(Tonttisuunnitelma suunnitelma) {
        
        Transaction transaktio = null;
        try (Session istunto = istuntotehdas.openSession()){
            List<Tonttisuunnitelma> sList = getSuunnitelmaTaulu();
        for(Tonttisuunnitelma s :sList){
            if(s.getOmistajaId() == suunnitelma.getOmistajaId() && s.getNimi().equals(suunnitelma.getNimi())){               
                //Päivitetään olemassaolevaa suunnitelmaa
                transaktio = istunto.beginTransaction();
//                System.out.println("@@@@@@@@@@@@@@@");
                suunnitelma.setJaettu(false);//Ei jaeta uutta suunnitelmaa vaikka edellinen olisikin jaettu
                istunto.saveOrUpdate(suunnitelma);
                transaktio.commit();
                return true;
            }
        }
        suunnitelma.setJaettu(false);//Ei jaeta uutta suunnitelmaa vaikka edellinen olisikin jaettu
        //Tallennetaan uusi suunnitelma
        transaktio = istunto.beginTransaction();
        istunto.save(suunnitelma);
        transaktio.commit();
        return true;
            
        } catch(Exception e) {
            System.out.println("Suunnitelman tallenntamisessa ongelmia");
            e.printStackTrace();
            if (transaktio!=null){
                transaktio.rollback();
            }
            return false;
        }
    }
    public boolean jaaSuunnitelma (String suunnitelmaNimi, String omistaja) {
         int id = getSuunnitelmaId(suunnitelmaNimi,omistaja);
         if(id == 0){
             System.out.println("Suunnitelmaa ei löytynyt jaettavaksi");
             return false;
         }
         
         try (Session istunto = istuntotehdas.openSession()){
            Transaction transaktio = istunto.beginTransaction();
            Tonttisuunnitelma suunnitelma = (Tonttisuunnitelma)istunto.load(Tonttisuunnitelma.class, id);
            
//            //Suunnitelmaan asetetaan sisältö ennen tallentamiata (Muuten sisältö häviää)
//            ArrayList<String[]> sisalto = suunnitelma.haeSisaltoBlob();
//             System.out.println("Sisalto size: "+sisalto.size());
//            suunnitelma.setBlobArray(sisalto);
//            suunnitelma.asetaSisalto(sisalto);
            
            suunnitelma.setJaettu(true);
            istunto.saveOrUpdate(suunnitelma);
            transaktio.commit();
            return true;
         } catch (Exception e) {
             System.out.println("Suunnitelman jakamisessa ongelmia");
             e.printStackTrace();
             return false;
         }
     }
     public boolean jaaSuunnitelma (Tonttisuunnitelma tontti) {
         try (Session istunto = istuntotehdas.openSession()){
             
            Transaction transaktio = istunto.beginTransaction();
            //Suunnitelmaan asetetaan sisältö ennen tallentamiata (Muuten sisältö häviää)
            tontti.asetaSisalto(tontti.haeSisaltoBlob());
            tontti.setJaettu(true);
            
            istunto.saveOrUpdate(tontti);
            transaktio.commit();
            return true;
         } catch (Exception e) {
             System.out.println("Suunnitelman jakamisessa ongelmia");
             e.printStackTrace();
             return false;
         }
     }
}
