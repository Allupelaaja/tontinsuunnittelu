package Model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import javax.persistence.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */

@Entity
@Table(name="tonttisuunnitelmat")
public class Tonttisuunnitelma {
    private int id;
    private Tontinsuunnittelija omistaja;
    private String nimi;
    private String alusta;
    private double leveys;
    private double korkeus;
    private double budjetti;
    private ArrayList<String[]> sisalto;
    private ArrayList<String[]> muutoslista;
    private int[] muutosmaara;
    private int muutosindeksi;
    private byte[] blobArray;
    private byte[] kuva;
    private boolean jaettu;

    
    
//    public Tonttisuunnitelma(Tontinsuunnittelija omistaja) {
//        this.omistaja = omistaja;
//    }
    public Tonttisuunnitelma(){
        blobArray = new byte[0];
        sisalto = new ArrayList<>();
        muutoslista = new ArrayList();
        muutosmaara = new int[100];
        muutosindeksi = 0;
        nimi = "";
        jaettu = false;
        
    }
    @Column(name="jaettu")
    public boolean isJaettu() {
        return jaettu;
    }

    public void setJaettu(boolean jaettu) {
        this.jaettu = jaettu;
    }
    
    @Column(name="kuva")
    public byte[] getKuva() {
        return kuva;
    }

    public void setKuva(byte[] kuva) {
        this.kuva = kuva;
    }
    @Transient
    public String getOmistajaNimi() {
        return omistaja.getNimi();
    }
    @Transient
    public int getOmistajaId() {
        return omistaja.getId();
    }
    /**
     *
     * @param omistaja
     */
    
    @Column(name="nimi")
    public String getNimi() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }
    @ManyToOne
    @JoinColumn(name="omistaja")
    public Tontinsuunnittelija getOmistaja() {
        return omistaja;
    }

    public void setOmistaja(Tontinsuunnittelija omistaja) {
        this.omistaja = omistaja;
    }
//    public ArrayList<String[]> getSisalto() {
//        return sisalto;
//    }
//
    
//    public void setSisalto(byte[] bytes) {
//        this.sisalto = blobToArr(bytes);
//    }
    @Column(name="blobArray")
    public byte[] getBlobArray() {
        return arrToBlob(sisalto);
//          return blobArray;
    }
    public void setBlobArray(byte[] blobArray){
        this.blobArray = blobArray;
    }
    public void setBlobArray(ArrayList<String[]> list) {
        this.blobArray = arrToBlob(list);
    }

    public ArrayList<String[]> haeSisaltoBlob() {
        return blobToArr(blobArray);
    }
//    public ArrayList<String[]> haeSisaltoArray() {
//        return sisalto;
//    }
    public void asetaSisalto(ArrayList<String[]> sisalto) {
        this.sisalto = sisalto;
    }
    public byte[] haeBlobArray() {
        return blobArray;
    }
    
    private byte[] arrToBlob (ArrayList<String[]> list){
        ByteArrayOutputStream baos;
        DataOutputStream out;
        try{
            baos = new ByteArrayOutputStream();
            out = new DataOutputStream(baos);
            for (String[] arr : list) {
                out.writeUTF(String.join(",", arr));
            }
            return baos.toByteArray();
        }catch (IOException e){
            e.printStackTrace();
            return null;
        } 
    }
    private ArrayList<String[]> blobToArr (byte[] bytes){
        try {
            ArrayList<String[]> resultArr = new ArrayList();
            ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
            DataInputStream in = new DataInputStream(bais);
            while (in.available() > 0) {
                String element = in.readUTF();
                String[] row = element.split(",");
                resultArr.add(row);
            }
            return resultArr;
        } catch (IOException e) {
            return null;
        }
    }
   
    
    /**
     *
     * @return
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    public int getId() {
        return id;
    }
    
    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }
    
    /**
     *
     * @return
     */
    @Column(name="leveys")
    public double getLeveys() {
        return leveys;
    }
    
    /**
     *
     * @param leveys
     */
    public void setLeveys(double leveys) {
        this.leveys = leveys;
    }
    
    /**
     *
     * @return
     */
    @Column(name="korkeus")
    public double getKorkeus() {
        return korkeus;
    }

    /**
     *
     * @param korkeus
     */
    public void setKorkeus(double korkeus) {
        this.korkeus = korkeus;
    }
    
//    /**
//     *
//     * @return
//     */
//    @Column(name="koko")
//    public double getKoko() {
//        return koko;
//    }
//
//    /**
//     *
//     * @param koko
//     */
//    public void setKoko(double koko) {
//        this.koko = koko;
//    }
    
//    /**
//     *
//     * @return
//     */
//    @Column(name="muoto")
//    public int getMuoto() {
//        return muoto;
//    }
//
//    /**
//     *
//     * @param muoto
//     */
//    public void setMuoto(int muoto) {
//        this.muoto = muoto;
//    }
    
    /**
     *
     * @return
     */
    @Column(name="alusta")
    public String getAlusta() {
        return alusta;
    }

    public void setAlusta(String alusta) {
        this.alusta = alusta;
    }
    
//    /**
//     *
//     * @return
//     */
//    @ManyToOne
//    @JoinColumn(name="TonttisuunnitelmaElementti")
//    public Elementti getElementti() {
//        return elementti;
//    }
//
//    
//    
//    /**
//     *
//     * @param elementti
//     */
//    public void setElementti(Elementti elementti) {
//        this.elementti = elementti;
//    }
    @Column(name="budjetti")
    public double getBudjetti() {
        return budjetti;
    }
    public void setBudjetti (double budjetti) {
        this.budjetti = budjetti;
    }
    public double lisaa(double hinta) {
        budjetti = budjetti - hinta;
        return budjetti;
    }
    public double palauta(double hinta) {
        budjetti = budjetti + hinta;
        return budjetti;
    }
    public void lisaaRuutuun(String x, String y, String alusta, String elementti) {
        String[] valittu = {x, y, alusta, elementti};
        boolean sisaltoa = false;
        if (sisalto != null) {
            for (int i = 0; i < sisalto.size(); i++) {
                String[] ruutu = sisalto.get(i);
                if (valittu[0].equals(ruutu[0]) && valittu[1].equals(ruutu[1])) {
                    sisaltoa = true;
                    if (valittu[2] == null && valittu[3] == null) {                       
                        sisalto.remove(i);
                    } else if (valittu[2] == null) {
                        valittu[2] = ruutu[2];
                        sisalto.set(i, valittu);
                    } else {
                        valittu[3] = ruutu[3];
                        sisalto.set(i, valittu);
                    }
                }
            }
        }
        if (!sisaltoa) {
            if (valittu[2] == null && valittu[3] == null) {
            } else {
                sisalto.add(valittu);
            }
        }
        muutoslista.add(valittu);
        // Kannattaa olla pois käytöstä ellei ole tarvista 
        // (Elementtien lisäys on suluvampaa ilman tätä)
        // testausta varten näyttää ArrayListin sisällön
//        for (int i = 0; i < sisalto.size(); i++) {
//            String[] ruutu = sisalto.get(i);
//            for (int j = 0; j < ruutu.length; j++) {
//                System.out.print(ruutu[j]+ " ");
//            }
//            System.out.print("\n");
//        }
//        System.out.print("\n");
    }
    
    public void vaihdaRuutu(String x, String y, String alusta, String elementti) {
        String[] valittu = {x, y, alusta, elementti};
        boolean sisaltoa = false;
        for (int i = 0; i < sisalto.size(); i++) {
            String[] ruutu = sisalto.get(i);
            if (valittu[0].equals(ruutu[0]) && valittu[1].equals(ruutu[1])) {
                sisaltoa = true;
                if (valittu[2] == null && valittu[3] == null) {                       
                    sisalto.remove(i);
                } else {
                    sisalto.set(i, valittu);
                }
            }
        }
        if (!sisaltoa) {
            if (valittu[2] == null && valittu[3] == null) {
            } else {
                sisalto.add(valittu);
            }
        }
        muutoslista.add(valittu);
        // Kannattaa olla pois käytöstä ellei ole tarvista 
        // (Elementtien lisäys on suluvampaa ilman tätä)
        // testausta varten näyttää ArrayListin sisällön
//        for (int i = 0; i < sisalto.size(); i++) {
//            String[] ruutu = sisalto.get(i);
//            for (int j = 0; j < ruutu.length; j++) {
//                System.out.print(ruutu[j]+ " ");
//            }
//            System.out.print("\n");
//        }
//        System.out.print("\n");
    }
    
    public String[] haeRuudunKomponentit(int x, int y) {
        String[] ruudunKomponentit = {null, null};
        if (sisalto != null) {
            for (int i = 0; i < sisalto.size(); i++) {
                String[] ruutu = sisalto.get(i);
                if (x == Integer.parseInt(ruutu[0]) && y == Integer.parseInt(ruutu[1])) {
                    ruudunKomponentit[0] = ruutu[2];
                    ruudunKomponentit[1] = ruutu[3];
                    return ruudunKomponentit;
                }
            }
        }
        return ruudunKomponentit;
    }
    
    public void tallennaMuutosmaara() {
        if (muutosindeksi == 100) {
            muutosindeksi = 0;
        }
        muutosmaara[muutosindeksi] = muutoslista.size();
        muutosindeksi++;
    }
    
    public int haeMuutosmaara() {
        muutosindeksi--;
        int tulos;
        if (muutosindeksi == 0) {
            tulos = muutosmaara[0] - muutosmaara[99];
        } else {
            tulos = muutosmaara[muutosindeksi] - muutosmaara[muutosindeksi - 1];
        }
        return tulos;
    }
    
    public ArrayList haeMuutoslista() {
        return muutoslista;
    }
    
    public void lisaaMuutosindeksiin(int muutoksia) {
        muutosindeksi = muutosindeksi + muutoksia;
    }
    
}
