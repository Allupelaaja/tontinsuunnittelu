/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;
import javax.persistence.*;
/**
 *
 * @author Allupelaaja
 */
@Entity
@Table(name="kayttajat")
public class Tontinsuunnittelija {
    private static final int VIERAILIJA = 0,PERUSKAYTTAJA = 1,TARKISTAJA = 2, YLLAPITAJA = 3;
    private String nimi;
    private int salasana;
    private int id;
    private int oikeudet;
    
    /**
     *
     */
    public Tontinsuunnittelija() {
        
    }

    /**
     *
     * @param nimi
     * @param salasana
     */
    public Tontinsuunnittelija(String nimi, int salasana) {
        this.nimi = nimi;
        this.salasana = salasana;
        this.oikeudet = PERUSKAYTTAJA;
    }

    /**
     *
     * @return
     */
    @Column(name="salasana")
    public int getSalasana() {
        return salasana;
    }

    /**
     *
     * @return
     */
    @Column(name="nimi")
    public String getNimi() {
        return nimi;
    }
    /**
     *
     * @return
     */
    @Column(name="oikeudet")
    public int getOikeudet() {
        return oikeudet;
    }

    /**
     *
     * @return
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    public int getId() {
        return id;
    }

    /**
     *
     * @param nimi
     */
    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @param oikeudet
     */
    public void setOikeudet(int oikeudet) {
        this.oikeudet = oikeudet;
    }
    
    /**
     *
     * @param salasana
     */
    public void setSalasana(int salasana) {
        this.salasana = salasana;
    }
    
    
    
}
