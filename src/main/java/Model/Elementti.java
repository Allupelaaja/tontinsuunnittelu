/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;


import javax.persistence.*;

/**
 *
 * @author Allupelaaja
 */
@Entity
@Table(name="elementit")
public class Elementti {
    public static final int ALUSTA = 1, AITA = 2, KASVI = 3;
    private int id;
    private String nimi;
    private int tyyppi;
    private String kuvanNimi;
    private byte[] kuva;
    //private int sijaintiX;
    //private int sijaintiY;
    //private Materiaali materiaali;

    public Elementti(String nimi, int tyyppi, String kuvanNimi, byte[] kuva) {
        this.nimi = nimi;
        this.tyyppi = tyyppi;
        this.kuvanNimi = kuvanNimi;
        this.kuva = kuva;
    }
    @Column(name="kuvanNimi")
    public String getKuvanNimi() {
        return kuvanNimi;
    }

    public void setKuvanNimi(String kuvanNimi) {
        this.kuvanNimi = kuvanNimi;
    }
    @Column(name="kuva")
    public byte[] getKuva() {
        return kuva;
    }

    public void setKuva(byte[] kuva) {
        this.kuva = kuva;
    }
    
    

    
    
    /**
     *
     */
    public Elementti () {
        
    }

    
//    public Elementti(String nimi, int sijaintiX, int sijaintiY, Materiaali materiaali) {
//        this.nimi = nimi;
//        this.sijaintiX = sijaintiX;
//        this.sijaintiY = sijaintiY;
//        this.materiaali = materiaali;
//    }
    
//    public Elementti(String nimi){
//        this.nimi = nimi;
//        this.sijaintiX = 0;
//        this.sijaintiY = 0;
//        this.materiaali = new Materiaali(1,"koivu",5);
//    }
    
    /**
     *
     * @return
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    /**
     *
     * @return
     */
    @Column(name="nimi")
    public String getNimi() {
        return nimi;
    }
    /**
     *
     * @param nimi
     */
    public void setNimi(String nimi) {
        this.nimi = nimi;
    }
    @Column(name="tyyppi")
    public int getTyyppi() {
        return tyyppi;
    }

    public void setTyyppi(int tyyppi) {
        this.tyyppi = tyyppi;
    }
}
