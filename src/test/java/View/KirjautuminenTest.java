///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package View;
//
//import javafx.stage.Stage;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//import static org.testfx.api.FxAssert.verifyThat;
//import org.testfx.framework.junit.ApplicationTest;
//import static org.testfx.matcher.control.LabeledMatchers.hasText;
//import org.testfx.matcher.control.TextMatchers;
//
///**
// *
// * @author timok
// */
//public class KirjautuminenTest extends ApplicationTest {
//    
//    Kayttoliittyma kayttis;
//    @Override 
//    public void start(Stage stage) {
//        kayttis = new Kayttoliittyma();
//        kayttis.start(stage);
//        kayttis.showKirjauduStage();
//    }
//    
//    public KirjautuminenTest() {
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//    }
//    
//    @After
//    public void tearDown() {
//    }
//    
//    @Test
//    public void vaara_salasana() {
//        clickOn("#userTextField");
//        write("timo");
//        clickOn("#pwBox");
//        write("salasana");
//        clickOn("#loginBtn");
//        
//        assertTrue(kayttis.kirjaudu.getStage().isShowing());
//    }
//    
//    @Test
//    public void vaara_kayttajatunnus() {
//        clickOn("#userTextField");
//        write("admin");
//        clickOn("#pwBox");
//        write("padmin");
//        clickOn("#loginBtn");
//        
//        assertTrue(kayttis.kirjaudu.getStage().isShowing());
//    }
//
//    @Test
//    public void oikea_salasana() {
//        clickOn("#userTextField");
//        write("timo");
//        clickOn("#pwBox");
//        write("qwerty");
//        clickOn("#loginBtn");
//        
//        assertTrue(kayttis.etu.getStage().isShowing());
//    }
//    
//}
