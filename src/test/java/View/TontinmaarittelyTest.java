///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package View;
//
//import javafx.stage.Stage;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//import static org.testfx.api.FxAssert.verifyThat;
//import org.testfx.framework.junit.ApplicationTest;
//import static org.testfx.matcher.control.LabeledMatchers.hasText;
//
///**
// *
// * @author Allupelaaja
// */
//public class TontinmaarittelyTest extends ApplicationTest {
//    
//    Kayttoliittyma sivu;
//    @Override 
//    public void start(Stage stage) {
//        sivu = new Kayttoliittyma();
//        sivu.start(stage);
//        sivu.showKirjauduStage();
//    }
//    
//    public TontinmaarittelyTest() {
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//        System.out.println("Aloitetaan testaus");
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//        System.out.println("Lopetetaan testaus");
//    }
//    
//    @Before
//    public void setUp() {
//        System.out.println("Kirjaudutaan sisään");
//        
//        clickOn("#userTextField");
//        write("testiKayttaja");
//        clickOn("#pwBox");
//        write("testiKayttaja");
//        clickOn("#loginBtn");
//    }
//    
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of annaBudjetti method, of class Tontinmaarittely.
//     */
//    @Test
//    public void testAnnaBudjetti() {
//        System.out.println("Luodaan uusi tontti");
//        verifyThat("#planBtn", hasText("Uusi suunnitelma"));
//        clickOn("#planBtn");
//        
//        System.out.println("Asetetaan pinta-alaksi 100");
//        clickOn("#sizeField");
//        write("100");
//        
//        System.out.println("Asetetaan leveydeksi 10");
//        clickOn("#widthField");
//        write("10");
//        
//        System.out.println("Asetetaan korkeudeksi 10");
//        clickOn("#heightField");
//        write("10");
//        
//        System.out.println("Klikataan hyväksymisnappia");
//        clickOn("#acceptBtn");
//        
//        System.out.println("Tarkistetaan onko tonttinäkymä esillä");
//        System.out.println(sivu.tonttiNakyma.getStage().isShowing());
//        assertTrue(sivu.tonttiNakyma.getStage().isShowing());
//    }
//    
//}
