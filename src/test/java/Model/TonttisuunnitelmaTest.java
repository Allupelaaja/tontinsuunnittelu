/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author JukkaPC
 */
public class TonttisuunnitelmaTest {
    
    public TonttisuunnitelmaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class Tonttisuunnitelma.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        Tonttisuunnitelma instance = new Tonttisuunnitelma();
        int expResult = 0;
        int result = instance.getId();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setId method, of class Tonttisuunnitelma.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        int id = 0;
        Tonttisuunnitelma instance = new Tonttisuunnitelma();
        instance.setId(id);
        
    }

    /**
     * Test of getLeveys method, of class Tonttisuunnitelma.
     */
    @Test
    public void testGetLeveys() {
        System.out.println("getLeveys");
        Tonttisuunnitelma instance = new Tonttisuunnitelma();
        double expResult = 0.0;
        double result = instance.getLeveys();
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of setLeveys method, of class Tonttisuunnitelma.
     */
    @Test
    public void testSetLeveys() {
        System.out.println("setLeveys");
        double leveys = 0.0;
        Tonttisuunnitelma instance = new Tonttisuunnitelma();
        instance.setLeveys(leveys);
        
    }

    /**
     * Test of getKorkeus method, of class Tonttisuunnitelma.
     */
    @Test
    public void testGetKorkeus() {
        System.out.println("getKorkeus");
        Tonttisuunnitelma instance = new Tonttisuunnitelma();
        double expResult = 0.0;
        double result = instance.getKorkeus();
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of setKorkeus method, of class Tonttisuunnitelma.
     */
    @Test
    public void testSetKorkeus() {
        System.out.println("setKorkeus");
        double korkeus = 0.0;
        Tonttisuunnitelma instance = new Tonttisuunnitelma();
        instance.setKorkeus(korkeus);
        
    }
    /**
     * Test of getAlusta method, of class Tonttisuunnitelma.
     */
    @Test
    public void testGetAlusta() {
        System.out.println("getAlusta");
        Tonttisuunnitelma instance = new Tonttisuunnitelma();
        //int expResult = 0;
        String result = instance.getAlusta();
        assertEquals(null, result);
        
    }

    /**
     * Test of setAlusta method, of class Tonttisuunnitelma.
     */
    @Test
    public void testSetAlusta() {
        System.out.println("setAlusta");
        String alusta = "";
        Tonttisuunnitelma instance = new Tonttisuunnitelma();
        instance.setAlusta(alusta);
        assertEquals("", instance.getAlusta());
    }

    
}
