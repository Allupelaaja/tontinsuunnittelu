/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author JukkaPC
 */
public class MateriaaliTest {
    
    public MateriaaliTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test class Materiaali.
     */
     @Test
    public void testMateriaali() {
        System.out.println("testMateriaali");
        Materiaali instance = new Materiaali("testi",1.5,"testi",1,new byte[1]);
        assertEquals("testi", instance.getNimi());
        assertEquals(1.5, instance.getHinta(),0.01);
        assertEquals("testi", instance.getKuvanNimi());
    }
    @Test
    public void testGetNimi() {
        System.out.println("getNimi");
        Materiaali instance = new Materiaali();
        String expResult = null;
        String result = instance.getNimi();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setNimi method, of class Materiaali.
     */
    @Test
    public void testSetNimi() {
        System.out.println("setNimi");
        String nimi = "";
        Materiaali instance = new Materiaali();
        instance.setNimi(nimi);
        
    }

    /**
     * Test of getHinta method, of class Materiaali.
     */
    @Test
    public void testGetHinta() {
        System.out.println("getHinta");
        Materiaali instance = new Materiaali();
        double expResult = 0.0;
        double result = instance.getHinta();
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of setHinta method, of class Materiaali.
     */
    @Test
    public void testSetHinta() {
        System.out.println("setHinta");
        double hinta = 0.0;
        Materiaali instance = new Materiaali();
        instance.setHinta(hinta);
        
    }

    /**
     * Test of getId method, of class Materiaali.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        Materiaali instance = new Materiaali();
        int expResult = 0;
        int result = instance.getId();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setId method, of class Materiaali.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        int id = 0;
        Materiaali instance = new Materiaali();
        instance.setId(id);
        
    }
    
}
