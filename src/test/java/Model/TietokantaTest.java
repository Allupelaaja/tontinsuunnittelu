/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import View.Luokayttaja;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Allupelaaja
 */
public class TietokantaTest {
    private String nimi = "ttttttt";
    private String salasana = "ttttttt";
    public TietokantaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        System.out.println("Ajetaan testi...");
        nimi = "ttttttt";
        salasana = "ttttttt";
    }
    
    @After
    public void tearDown() {
        Tietokanta tk = new Tietokanta();
        tk.poistaKayttaja(nimi);
        System.out.println("Testi loppuu...");
    }
    
   @org.junit.Test
    public void testSalaa() {
        System.out.println("salaa");
        
        Tietokanta instance = new Tietokanta();
        int expResult = 30518208;
        System.out.println(instance.salaa(salasana));
        int result = instance.salaa(salasana);
        assertEquals(expResult, result);
    }

    /**
     * Test of luoKayttaja method, of class Tietokanta.
     */
    @Test
    public void testLuoKayttaja() {
        System.out.println("luoKayttaja");
        
        Tietokanta instance = new Tietokanta();
        boolean result = instance.luoKayttaja(nimi, salasana);
        boolean expResult = true;
        assertEquals(expResult, result);
        assertEquals(false, instance.luoKayttaja(nimi, salasana));
        System.out.println("Kayttajan poistaminen");
        instance.poistaKayttaja(nimi);
        assertEquals(true,instance.getKayttajaId(nimi) == 0);
        System.out.println("Olemattoman kayttajan poisto");
        instance.poistaKayttaja(nimi);
        assertEquals(true,instance.getKayttajaId(nimi) == 0);
        System.out.println("Admin kayttajan poisto");
        assertEquals(false, instance.poistaKayttaja("admin"));
        System.out.println("Tyhjan nimen kayttajan luonti");
        assertEquals(false,instance.luoKayttaja("", salasana));
        System.out.println("Tyhjan salasanan kayttajan luonti");
        assertEquals(false,instance.luoKayttaja(nimi,""));
        
        
    }

    /**
     * Test of jaaSuunnitelma method, of class Tietokanta.
     */
    @Test
    public void testPäivitäOikeudet() {
        System.out.println("paivitaOikeudet");
        
       
        
        int oikeus = 0;
        Tietokanta instance = new Tietokanta();
        instance.luoKayttaja(nimi, salasana);
        
        boolean expResult = instance.päivitäOikeudet(nimi, oikeus);
        assertEquals(true, expResult);
        instance.poistaKayttaja(nimi);
        System.out.println("ei olemassa olevan kayttajan oikeuksien muuttaminen");
        assertEquals(false, instance.päivitäOikeudet(nimi, oikeus));
    }

    /**
     * Test of vaihdaSalasana method, of class Tietokanta.
     */
    @Test
    public void testVaihdaSalasana() {
        System.out.println("vaihdaSalasana");
       
        String vanha_salasana = "testiKayttaja";
        String uusi_salasana = "testiKayttajaUusi";
        Tietokanta instance = new Tietokanta();
        System.out.println("ei olemassa olevan kayttajan salasanan vaihto");
        assertEquals(false, instance.vaihdaSalasana(nimi, vanha_salasana, uusi_salasana));
        instance.luoKayttaja(nimi, vanha_salasana);
        System.out.println("kayttajan antama vanja salasana ei ole oikea");
        assertEquals(false, instance.vaihdaSalasana(nimi, uusi_salasana, uusi_salasana));
        boolean expResult = true;
        boolean result = instance.vaihdaSalasana(nimi, vanha_salasana, uusi_salasana);
        assertEquals(expResult, result);
        instance.poistaKayttaja(nimi);
    }

    /**
     * Test of vaihdaNimi method, of class Tietokanta.
     */
    @Test
    public void testVaihdaNimi() {
        System.out.println("vaihdaNimi");
        String vanha = nimi;
        String uusi = nimi+"uusi";
        Tietokanta instance = new Tietokanta();
        System.out.println("ei olemassa olevan kayttajan nimen vaihto");
        assertEquals(false, instance.vaihdaNimi(vanha, uusi));
        instance.luoKayttaja(vanha, "salasana");
        boolean expResult = true;
        boolean result = instance.vaihdaNimi(vanha, uusi);
        assertEquals(expResult, result);
        instance.poistaKayttaja(uusi);
        instance.poistaKayttaja(vanha);
    }

    /**
     * Test of kirjauduSisaan method, of class Tietokanta.
     */
    @Test
    public void testKirjauduSisaan() {
        System.out.println("kirjauduSisaan");
        Tietokanta instance = new Tietokanta();
        System.out.println("ei olemassa olevan kayttajan kirjautuminen");
        assertEquals(false, instance.kirjauduSisaan(nimi, salasana));
        instance.luoKayttaja(nimi, salasana);
        boolean expResult = true;
        boolean result = instance.kirjauduSisaan(nimi, salasana);
        assertEquals(expResult, result);
        instance.poistaKayttaja(nimi);
        System.out.println("kayttaja vaara salasana kirjauuminen");
        assertEquals(false, instance.kirjauduSisaan(nimi, salasana));
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getKayttajaOikeus method, of class Tietokanta.
     */
    @Test
    public void testGetKayttajaOikeus() {
        System.out.println("getKayttajaOikeus");
        Tietokanta instance = new Tietokanta();
        instance.luoKayttaja(nimi, nimi);
        int expResult = 1;
        int result = instance.getKayttajaOikeus(nimi);
        assertEquals(expResult, result);
        instance.poistaKayttaja(nimi);
        assertEquals(-1, instance.getKayttajaOikeus(nimi));
    }
}
